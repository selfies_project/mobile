package hu.zsomii.selfies.interactor;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import hu.zsomii.selfies.dagger.DaggerTestHelper;
import hu.zsomii.selfies.interactor.editprofile.EditProfileInteractor;
import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.stub.ModifyUserDto;
import hu.zsomii.selfies.stub.User;

public class EditProfileInteractorUnitTests {
    private EditProfileInteractor editProfileInteractor;
    private ArrayList<ProfileFields> errors;

    @Before
    public void setUp() {
        DaggerTestHelper.initDagger();
        editProfileInteractor = new EditProfileInteractor();
    }

    @Test
    public void editProfileValidation_allOkay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        ModifyUserDto testUser = createTestUser("ed1lord1@gmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertTrue(errors.isEmpty());
    }


    @Test
    public void editProfileValidation_invalidEmail() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        ModifyUserDto testUser = createTestUser("ed1lordgmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.contains(ProfileFields.EMAIL));
    }


    @Test
    public void editProfileValidation_emptyEmail() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        ModifyUserDto testUser = createTestUser("", "Adam", "Varga", birthDay.toString(), "Hungary", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.contains(ProfileFields.EMAIL));
    }

    @Test
    public void editProfileValidation_emptyFirstName() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        ModifyUserDto testUser = createTestUser("ed1lord@gmail.com", "", "Varga", birthDay.toString(), "Hungary", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.FIRST_NAME));
    }

    @Test
    public void editProfileValidation_emptyLastName() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        ModifyUserDto testUser = createTestUser("ed1lord@gmail.com", "Adam", "", birthDay.toString(), "Hungary", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.contains(ProfileFields.LAST_NAME));
    }

    @Test
    public void editProfileValidation_emptyBirthday() {
        ModifyUserDto testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", "", "Hungary", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.contains(ProfileFields.BIRTHDAY));
    }

    @Test
    public void editProfileValidation_emptyCountry() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        ModifyUserDto testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", birthDay.toString(), "", "", "");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.contains(ProfileFields.COUNTRY));
    }

    @Test
    public void editProfileValidation_invalidOldPassword() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        //password should be at least 5 chars long
        ModifyUserDto testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "lol","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.contains(ProfileFields.PASSWORD));
    }


    @Test
    public void editProfileValidation_allEmpty() {
        ModifyUserDto testUser = createTestUser("", "", "", null, "", "","");
        errors = editProfileInteractor.validateData(testUser);

        Assert.assertTrue(errors.contains(ProfileFields.EMAIL));
        Assert.assertTrue(errors.contains(ProfileFields.FIRST_NAME));
        Assert.assertTrue(errors.contains(ProfileFields.LAST_NAME));
        Assert.assertTrue(errors.contains(ProfileFields.BIRTHDAY));
        Assert.assertTrue(errors.contains(ProfileFields.COUNTRY));
    }


    private ModifyUserDto createTestUser(String email, String firstName, String lastName, String birthDate, String country, String oldPassword, String newPassword) {

        ModifyUserDto user = new ModifyUserDto();

        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setBirthDate(birthDate);
        user.setCountry(country);
        user.setOldPassword(oldPassword);
        user.setNewPassword(oldPassword);

        return user;
    }

}
