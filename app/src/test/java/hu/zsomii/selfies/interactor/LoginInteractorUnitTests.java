package hu.zsomii.selfies.interactor;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import hu.zsomii.selfies.dagger.DaggerTestHelper;
import hu.zsomii.selfies.interactor.login.LoginInteractor;
import hu.zsomii.selfies.presentation.screen.login.LoginFields;

public class LoginInteractorUnitTests {

    private LoginInteractor loginInteractor;
    private ArrayList<LoginFields> failedLoginFields;

    @Before
    public void setUp() {
        DaggerTestHelper.initDagger();
        loginInteractor = new LoginInteractor();
    }

    @Test
    public void loginValidation_allOkay() {
        failedLoginFields = loginInteractor.invalidFields("ed1lord1@gmail.com", "Alma123!");
        Assert.assertTrue(failedLoginFields.isEmpty());
    }

    @Test
    public void loginValidation_invalidEmail() {
        failedLoginFields = loginInteractor.invalidFields("ed1lord1gmail.com", "Alma123!");
        Assert.assertTrue(failedLoginFields.size() == 1);
        Assert.assertTrue(failedLoginFields.contains(LoginFields.EMAIL));
    }

    @Test
    public void loginValidation_invalidPassword() {
        failedLoginFields = loginInteractor.invalidFields("ed1lord1@gmail.com", "lol");
        Assert.assertTrue(failedLoginFields.size() == 1);
        Assert.assertTrue(failedLoginFields.contains(LoginFields.PASSWORD));
    }

    @Test
    public void loginValidation_allEmpty() {
        failedLoginFields = loginInteractor.invalidFields("", "");
        Assert.assertTrue(failedLoginFields.size() == 2);
        Assert.assertTrue(failedLoginFields.contains(LoginFields.EMAIL));
        Assert.assertTrue(failedLoginFields.contains(LoginFields.PASSWORD));
    }

}
