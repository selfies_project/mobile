package hu.zsomii.selfies.interactor;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import hu.zsomii.selfies.dagger.DaggerTestHelper;
import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.interactor.register.RegisterInteractor;
import hu.zsomii.selfies.stub.User;

public class RegisterInteractorUnitTests {

    private RegisterInteractor registerInteractor;
    private ArrayList<ProfileFields> errors;

    @Before
    public void setUp() {
        DaggerTestHelper.initDagger();
        registerInteractor = new RegisterInteractor();
    }


    @Test
    public void registerValidation_allOkay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("ed1lord1@gmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.isEmpty());
    }


    @Test
    public void registerValidation_invalidEmail() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("ed1lordgmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.EMAIL));
    }


    @Test
    public void registerValidation_emptyEmail() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("", "Adam", "Varga", birthDay.toString(), "Hungary", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.EMAIL));
    }

    @Test
    public void registerValidation_emptyFirstName() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("ed1lord@gmail.com", "", "Varga", birthDay.toString(), "Hungary", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.FIRST_NAME));
    }

    @Test
    public void registerValidation_emptyLastName() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("ed1lord@gmail.com", "Adam", "", birthDay.toString(), "Hungary", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.LAST_NAME));
    }

    @Test
    public void registerValidation_emptyBirthday() {
        User testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", null, "Hungary", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.BIRTHDAY));
    }

    @Test
    public void registerValidation_emptyCountry() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", birthDay.toString(), "", "Alma123!");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.COUNTRY));
    }

    @Test
    public void registerValidation_emptyPassword() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        User testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.PASSWORD));
    }

    @Test
    public void registerValidation_invalidPassword() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, 1993);
        Date birthDay = cal.getTime();

        //password should be at least 5 chars long
        User testUser = createTestUser("ed1lord@gmail.com", "Adam", "Varga", birthDay.toString(), "Hungary", "lol");
        errors = registerInteractor.validateData(testUser);

        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.contains(ProfileFields.PASSWORD));
    }



    @Test
    public void registerValidation_allEmpty() {
        User testUser = createTestUser("", "", "", "", "", "");
        errors = registerInteractor.validateData(testUser);

        Assert.assertEquals(errors.contains(ProfileFields.EMAIL), true);
        Assert.assertEquals(errors.contains(ProfileFields.FIRST_NAME), true);
        Assert.assertEquals(errors.contains(ProfileFields.LAST_NAME), true);
        Assert.assertEquals(errors.contains(ProfileFields.BIRTHDAY), true);
        Assert.assertEquals(errors.contains(ProfileFields.COUNTRY), true);
        Assert.assertEquals(errors.contains(ProfileFields.PASSWORD), true);
    }


    private User createTestUser(String email, String firstName, String lastName, String birthDate, String country, String password) {

        User user = new User();

        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setBirthDate(birthDate);
        user.setCountry(country);
        user.setPassword(password);

        return user;
    }


}
