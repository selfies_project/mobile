package hu.zsomii.selfies;

import javax.inject.Singleton;

import dagger.Component;
import hu.zsomii.selfies.dagger.TestInteractorModule;
import hu.zsomii.selfies.dagger.TestPresentationModule;
import hu.zsomii.selfies.dagger.TestRepositoryModule;
import hu.zsomii.selfies.dagger.TestUtilsModule;

@Singleton
@Component(modules = {TestPresentationModule.class, TestInteractorModule.class, TestRepositoryModule.class, TestUtilsModule.class})
public interface TestComponent extends IComponent {

}