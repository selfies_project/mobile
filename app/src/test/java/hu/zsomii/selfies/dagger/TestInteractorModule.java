package hu.zsomii.selfies.dagger;

import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.zsomii.selfies.interactor.appinit.AppInitInteractor;
import hu.zsomii.selfies.interactor.appinit.IAppInitInteractor;
import hu.zsomii.selfies.interactor.discover.DiscoverInteractor;
import hu.zsomii.selfies.interactor.discover.IDiscoverInteractor;
import hu.zsomii.selfies.interactor.editprofile.EditProfileInteractor;
import hu.zsomii.selfies.interactor.editprofile.IEditProfileInteractor;
import hu.zsomii.selfies.interactor.location.ILocationInteractor;
import hu.zsomii.selfies.interactor.location.LocationInteractor;
import hu.zsomii.selfies.interactor.login.ILoginInteractor;
import hu.zsomii.selfies.interactor.login.LoginInteractor;
import hu.zsomii.selfies.interactor.profile.IProfileInteractor;
import hu.zsomii.selfies.interactor.profile.ProfileInteractor;
import hu.zsomii.selfies.interactor.register.IRegisterInteractor;
import hu.zsomii.selfies.interactor.register.RegisterInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.interactor.session.SessionInteractor;
import hu.zsomii.selfies.interactor.share.IShareInteractor;
import hu.zsomii.selfies.interactor.share.ShareInteractor;
import hu.zsomii.selfies.interactor.share.takepicture.ITakePictureInteractor;
import hu.zsomii.selfies.interactor.share.takepicture.TakePictureInteractor;
import hu.zsomii.selfies.interactor.toplist.IToplistInteractor;
import hu.zsomii.selfies.interactor.toplist.ToplistInteractor;


@Module
public class TestInteractorModule {


    public TestInteractorModule() {
    }

    @Provides
    @Singleton
    ILoginInteractor providesILoginInteractor() {
        return Mockito.mock(LoginInteractor.class);
    }

    @Provides
    @Singleton
    public IRegisterInteractor providesIRegisterInteractor() {
        return Mockito.mock(RegisterInteractor.class);
    }

    @Provides
    @Singleton
    IToplistInteractor providesIToplistInteractor() {
        return Mockito.mock(ToplistInteractor.class);
    }

    @Provides
    @Singleton
    IProfileInteractor providesIProfileInteractor() {
        return Mockito.mock(ProfileInteractor.class);
    }

    @Provides
    @Singleton
    ISessionInteractor providesISessionInteractor() {
        return Mockito.mock(SessionInteractor.class);
    }

    @Provides
    @Singleton
    IShareInteractor providesIShareInteractor() {
        return Mockito.mock(ShareInteractor.class);
    }

    @Provides
    @Singleton
    ITakePictureInteractor providesITakePictureInteractor() {
        return Mockito.mock(TakePictureInteractor.class);
    }

    @Provides
    @Singleton
    IDiscoverInteractor providesIDiscoverInteractor() {
        return Mockito.mock(DiscoverInteractor.class);
    }

    @Provides
    @Singleton
    IEditProfileInteractor providesIEditProfileInteractor() {
        return Mockito.mock(EditProfileInteractor.class);
    }

    @Provides
    @Singleton
    ILocationInteractor providesILocationInteractor() {
        return Mockito.mock(LocationInteractor.class);
    }

    @Provides
    @Singleton
    IAppInitInteractor providesIAppInitInteractor() {
        return Mockito.mock(AppInitInteractor.class);
    }


}
