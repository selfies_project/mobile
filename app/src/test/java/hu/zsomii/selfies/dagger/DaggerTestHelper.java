package hu.zsomii.selfies.dagger;

import hu.zsomii.selfies.DaggerTestComponent;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.TestComponent;

public class DaggerTestHelper {

    public static void initDagger() {
        TestComponent component =
                DaggerTestComponent.builder()
                        .testPresentationModule(new TestPresentationModule())
                        .testInteractorModule(new TestInteractorModule())
                        .testRepositoryModule(new TestRepositoryModule())
                        .testUtilsModule(new TestUtilsModule(Selfies.getContext()))
                        .build();
        Selfies.setInjector(component);
    }

}
