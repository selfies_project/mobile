package hu.zsomii.selfies.dagger;

import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.repository.preferences.SharedPref;

@Module
public class TestRepositoryModule {

    public TestRepositoryModule() {

    }

    @Provides
    @Singleton
    ISharedPref providesISharedPreferences() {
        return Mockito.mock(SharedPref.class);
    }
}
