package hu.zsomii.selfies.dagger;

import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.zsomii.selfies.interactor.register.RegisterInteractor;
import hu.zsomii.selfies.presentation.appinit.AppInitPresenter;
import hu.zsomii.selfies.presentation.appinit.IAppInitPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.DashboardPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.IDashboardPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.DiscoverListPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.IDiscoverListPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.IProfilePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.ProfilePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.ISharePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.SharePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.ITakePicturePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.TakePicturePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.IToplistPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.ToplistPresenter;
import hu.zsomii.selfies.presentation.screen.editprofile.EditProfilePresenter;
import hu.zsomii.selfies.presentation.screen.editprofile.IEditProfilePresenter;
import hu.zsomii.selfies.presentation.screen.fbregister.FbRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.fbregister.IFbRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.login.ILoginPresenter;
import hu.zsomii.selfies.presentation.screen.login.LoginPresenter;
import hu.zsomii.selfies.presentation.screen.otherprofile.IOtherProfilePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofile.OtherProfilePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.IOtherProfileAlbumPresenter;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.OtherProfileAlbumPresenter;
import hu.zsomii.selfies.presentation.screen.register.IRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.RegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.succes.ISuccesRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.succes.SuccesRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.splash.ISplashScreenPresenter;
import hu.zsomii.selfies.presentation.screen.splash.SplashScreenPresenter;
import hu.zsomii.selfies.presentation.screen.terms.ITermsPresenter;
import hu.zsomii.selfies.presentation.screen.terms.TermsPresenter;

@Module
public class TestPresentationModule {

    public TestPresentationModule() {

    }

    @Provides
    @Singleton
    ISplashScreenPresenter providesISplashScreenPresenter() {
        return Mockito.mock(SplashScreenPresenter.class);
    }

    @Provides
    @Singleton
    ITermsPresenter providesITermsPresenter() {
        return Mockito.mock(TermsPresenter.class);
    }

    @Provides
    @Singleton
    ILoginPresenter providesILoginPresenter() {
        return Mockito.mock(LoginPresenter.class);
    }

    @Provides
    @Singleton
    IRegisterPresenter providesIRegisterPresenter() {
        return Mockito.mock(RegisterPresenter.class);

    }

    @Provides
    @Singleton
    IDashboardPresenter providesIDashboardPresenter() {
        return Mockito.mock(DashboardPresenter.class);

    }

    @Provides
    @Singleton
    ISuccesRegisterPresenter providesISuccesRegisterPresenter() {
        return Mockito.mock(SuccesRegisterPresenter.class);

    }

    @Provides
    @Singleton
    IToplistPresenter providesIToplistPresenter() {
        return Mockito.mock(ToplistPresenter.class);

    }

    @Provides
    @Singleton
    IDiscoverListPresenter providesIDiscoverListPresenter() {
        return Mockito.mock(DiscoverListPresenter.class);

    }

    @Provides
    @Singleton
    IProfilePresenter providesIProfilePresenter() {
        return Mockito.mock(ProfilePresenter.class);

    }

    @Provides
    @Singleton
    ISharePresenter providesISharePresenter() {
        return Mockito.mock(SharePresenter.class);

    }

    @Provides
    @Singleton
    ITakePicturePresenter providesITakePicturePresenter() {
        return Mockito.mock(TakePicturePresenter.class);

    }

    @Provides
    @Singleton
    IEditProfilePresenter providesIEditProfilePresenter() {
        return Mockito.mock(EditProfilePresenter.class);

    }

    @Provides
    @Singleton
    IAppInitPresenter providesIAppInitPresenter() {
        return Mockito.mock(AppInitPresenter.class);

    }

    @Provides
    @Singleton
    IOtherProfilePresenter providesIOtherProfilePresenter() {
        return Mockito.mock(OtherProfilePresenter.class);

    }

    @Provides
    @Singleton
    IOtherProfileAlbumPresenter providesIOtherProfileAlbumPresenter() {
        return Mockito.mock(OtherProfileAlbumPresenter.class);

    }

    @Provides
    @Singleton
    IFbRegisterPresenter providesIFbRegisterPresenter() {
        return Mockito.mock(FbRegisterPresenter.class);

    }
}
