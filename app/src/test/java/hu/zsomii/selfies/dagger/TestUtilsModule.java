package hu.zsomii.selfies.dagger;

import android.content.Context;
import android.test.mock.MockContext;

import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

@Module
public class TestUtilsModule {

    private Context _context;

    public TestUtilsModule(Context context) {
        _context = context;
    }

    @Provides
    @Singleton
    ModelMapper providesObjectMapper() {
        return new ModelMapper();
    }

    @Provides
    @Singleton
    Context providesContext() {
        return new MockContext();
    }

    @Provides
    @Singleton
    EventBus providesEventBus() {
        return EventBus.getDefault();
    }
}
