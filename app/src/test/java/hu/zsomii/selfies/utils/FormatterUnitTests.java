package hu.zsomii.selfies.utils;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Calendar;

import hu.zsomii.selfies.globals.Formatter;

public class FormatterUnitTests {

    private static final int yearsToRemove = 24;
    private static final int dayModifier = 1;


    @Test
    public void ageValidation_BirthdayDayInPast() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -yearsToRemove);
        cal.add(Calendar.DAY_OF_YEAR, -dayModifier);

        Assert.assertTrue(Formatter.calculateAge(cal.getTime()) == yearsToRemove);
    }

    @Test
    public void ageValidation_BirthdayDayToday() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -yearsToRemove);
        cal.add(Calendar.DAY_OF_YEAR, 0);
        Assert.assertTrue(Formatter.calculateAge(cal.getTime()) == yearsToRemove);
    }

    @Test
    public void ageValidation_BirthdayDayTomorrow() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -yearsToRemove);
        cal.add(Calendar.DAY_OF_YEAR, dayModifier);

        Assert.assertTrue(Formatter.calculateAge(cal.getTime()) == yearsToRemove - 1);
    }
}
