package hu.zsomii.selfies.presentation.screen.dashboard.profile;

import hu.zsomii.selfies.stub.ProfileStat;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public interface IProfileFragment {
    void showUserData(User userData);
    void setProfileStats(int selfies, int score, int likes);
    void showProfileStat(ProfileStat profileStat);
    void goToEditProfile();
    void goToAlbum();
}
