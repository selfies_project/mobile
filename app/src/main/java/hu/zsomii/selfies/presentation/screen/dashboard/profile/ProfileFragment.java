package hu.zsomii.selfies.presentation.screen.dashboard.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.presentation.base.BaseFragment;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.presentation.screen.editprofile.EditProfileActivity;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.OtherProfileAlbumActivity;
import hu.zsomii.selfies.stub.ProfileStat;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.utils.GlideImageView;

/**
 * Created by Adam Varga on 2/18/2018.
 */


public class ProfileFragment extends BaseFragment implements IProfileFragment {

    @Inject
    IProfilePresenter presenter;

    CircleImageView civProfileImage;
    ImageView ivHeaderBackground;
    ImageView ivSelfiesIcon;
    ImageView ivScoreIcon;
    ImageView ivLikesIcon;
    TextView tvSelfies;
    TextView tvScore;
    TextView tvLikes;
    TextView tvName;
    TextView tvBirthday;
    TextView tvCountry;
    TextView tvEmail;
    RelativeLayout headerItemImages;

    Button btnEditProfile;
    Formatter formatter;

    public ProfileFragment() {
        Selfies.injector().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        formatter = new Formatter();
    }

    @SuppressLint({"SetTextI18n", "CutPasteId"})
    private void initComponents(View view) {
        civProfileImage = view.findViewById(R.id.civProfileImage);
        ivHeaderBackground = view.findViewById(R.id.ivHeaderBackground);
        headerItemImages =  view.findViewById(R.id.headerItemImages);

        tvName = view.findViewById(R.id.tvName);
        tvBirthday = view.findViewById(R.id.tvBirthday);
        tvCountry = view.findViewById(R.id.tvCountry);
        tvEmail = view.findViewById(R.id.tvEmail);

        ivSelfiesIcon = view.findViewById(R.id.headerItemImages).findViewById(R.id.ivItemIcon);
        ivScoreIcon = view.findViewById(R.id.headerItemScore).findViewById(R.id.ivItemIcon);
        ivLikesIcon = view.findViewById(R.id.headerItemLikes).findViewById(R.id.ivItemIcon);

        tvSelfies = view.findViewById(R.id.headerItemImages).findViewById(R.id.tvCount);
        tvScore = view.findViewById(R.id.headerItemScore).findViewById(R.id.tvCount);
        tvLikes = view.findViewById(R.id.headerItemLikes).findViewById(R.id.tvCount);

        btnEditProfile = view.findViewById(R.id.btnEditProfile);

        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.editButtonPressed();
            }
        });
    }

    @Override
    public void setProfileStats(int selfies, int score, int likes) {
        ivSelfiesIcon.setImageResource(R.drawable.camera);
        ivScoreIcon.setImageResource(R.drawable.star);
        ivLikesIcon.setImageResource(R.drawable.heart);

        tvSelfies.setText(String.valueOf(selfies));
        tvScore.setText(String.valueOf(score));
        tvLikes.setText(String.valueOf(likes));
    }

    @Override
    public void showProfileStat(ProfileStat profileStat) {
        setProfileStats(profileStat.getSelfieCount(), profileStat.getScore(), profileStat.getLikeCount());

        if (profileStat.getSelfieCount() > 0 ){
           headerItemImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.showAlbumClicked();
                }
            });
        }
    }

    @Override
    public void goToEditProfile() {
        startActivity(new Intent(getContext(), EditProfileActivity.class));
    }

    @Override
    public void goToAlbum() {
        startActivity(new Intent(getContext(), OtherProfileAlbumActivity.class));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showUserData(User userData) {
        tvName.setText(userData.getFirstName() + " " + userData.getLastName());
                 tvBirthday.setText(Formatter.formatBirthDayDate(Formatter.formatDate(userData.getBirthDate())));
        tvCountry.setText(userData.getCountry());
        tvEmail.setText(userData.getEmail());

        GlideImageView.loadImageById(getContext(), userData.getProfilePictureJson(), civProfileImage);
        GlideImageView.loadBlurImageById(getContext(), userData.getProfilePictureJson(), ivHeaderBackground, 50);
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

}