package hu.zsomii.selfies.presentation.screen.register.succes;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/8/2018.
 */

public class SuccesRegisterActivity extends BaseActivity implements ISuccesRegisterActivity {


    @Inject
    ISuccesRegisterPresenter presenter;

    Button btnGoToLogin;

    public SuccesRegisterActivity() {
        Selfies.injector().inject(this);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_succes);

        findViewById(R.id.btnGoToLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.goToLoginPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }
}
