package hu.zsomii.selfies.presentation.screen.register;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.utils.GlideImageView;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public class RegisterActivity extends BaseActivity implements IRegisterActivity {

    @Inject
    IRegisterPresenter presenter;

    TextInputEditText etFirst;
    TextInputLayout tilFirst;

    TextInputEditText etLast;
    TextInputLayout tilLast;

    TextInputEditText etEmail;
    TextInputLayout tilEmail;

    TextInputEditText etCountry;
    TextInputLayout tilCountry;

    TextInputEditText etBirthday;
    TextInputLayout tilBirthday;

    TextInputEditText etPassword;
    TextInputLayout tilPassword;

    Button btnRegister;
    Button btnTerms;

    ImageView ivProfilePictureBlurred;
    de.hdodenhof.circleimageview.CircleImageView ivProfilePicture;
    Formatter formatter;

    private static final int SELECT_PHOTO = 100;
    Bitmap takenPicture;
    boolean imageSelected = false;
    private String fbAccessToken;
    private Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Selfies.injector().inject(this);
        setContentView(R.layout.activity_register);
        formatter = new Formatter();

        activity = this;

        initViews();
        initInputLayouts();

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.termsClicked();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageSelected) {
                    presenter.registerClicked();
                } else {
                    showToast(getString(R.string.register_choose_picture));
                }
            }
        });


        ivProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.chooseImageClicked();
            }
        });

        tilBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(activity);
                showDatePickerDialog();
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(activity);
                showDatePickerDialog();
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null) {
            String email = getIntent().getExtras().getString(Globals.FB_REG_KEY_EMAIL);
            String firstName = getIntent().getExtras().getString(Globals.FB_REG_KEY_FIRST);
            String lastName = getIntent().getExtras().getString(Globals.FB_REG_KEY_LAST);
            String token = getIntent().getExtras().getString(Globals.FB_REG_KEY_TOKEN);

            if (email != null) {
                etEmail.setText(email);
            }

            if (firstName != null) {
                etFirst.setText(firstName);
            }

            if (lastName != null) {
                etLast.setText(lastName);
            }

            if (token != null) {
                fbAccessToken = token;
            }
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @SuppressLint("CutPasteId")
    private void initViews() {
        tilLast = findViewById(R.id.ifLast).findViewById(R.id.textInputLayout);
        etLast = findViewById(R.id.ifLast).findViewById(R.id.textInputEditText);
        tilFirst = findViewById(R.id.ifFirst).findViewById(R.id.textInputLayout);
        etFirst = findViewById(R.id.ifFirst).findViewById(R.id.textInputEditText);
        tilEmail = findViewById(R.id.ifEmail).findViewById(R.id.textInputLayout);
        etEmail = findViewById(R.id.ifEmail).findViewById(R.id.textInputEditText);
        tilCountry = findViewById(R.id.ifCountry).findViewById(R.id.textInputLayout);
        etCountry = findViewById(R.id.ifCountry).findViewById(R.id.textInputEditText);
        tilBirthday = findViewById(R.id.ifBirthday).findViewById(R.id.textInputLayout);
        etBirthday = findViewById(R.id.ifBirthday).findViewById(R.id.textInputEditText);
        etBirthday.setInputType(InputType.TYPE_NULL);
        tilPassword = findViewById(R.id.ifPassword).findViewById(R.id.textInputLayout);
        etPassword = findViewById(R.id.ifPassword).findViewById(R.id.textInputEditText);


        btnRegister = findViewById(R.id.btnRegister);
        btnTerms = findViewById(R.id.btnTerms);
        ivProfilePicture = findViewById(R.id.ivProfilePicture);
        ivProfilePictureBlurred = findViewById(R.id.ivProfilePictureBlurred);
    }

    private void initInputLayouts() {

        tilFirst.setHint(getString(R.string.register_first_name));
        tilLast.setHint(getString(R.string.register_last_name));
        tilEmail.setHint(getString(R.string.register_email));
        tilCountry.setHint(getString(R.string.register_country));
        tilBirthday.setHint(getString(R.string.register_birthday));
        tilPassword.setHint(getString(R.string.register_password));

    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }


    @Override
    public User getAllFieldsContent() {
        User user = new User();
        user.setFirstName(etFirst.getText().toString());
        user.setLastName(etLast.getText().toString());
        user.setEmail(etEmail.getText().toString());
        user.setCountry(etCountry.getText().toString());
        user.setBirthDate(etBirthday.getText().toString());
        user.setPassword(etPassword.getText().toString());
        user.setFbAccessToken(fbAccessToken);

        return user;
    }

    @Override
    public void takePicture() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        assert selectedImage != null;
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    takenPicture = getResizedBitmap(BitmapFactory.decodeStream(imageStream), 400);

                    GlideImageView.loadLocalImage(this, takenPicture, ivProfilePicture);
                    GlideImageView.loadLocalBlurImage(this, takenPicture, ivProfilePictureBlurred, 30);
                    imageSelected = true;
                }
        }
    }


    @Override
    public Bitmap getTakenPicture() {
        return takenPicture;
    }

    @Override
    public void showErrors(ArrayList<ProfileFields> fields) {
        for (int i = 0; i < fields.size(); i++) {
            switch (fields.get(i)) {
                case FIRST_NAME:
                    tilFirst.setError(getString(R.string.register_first_name_error));
                    break;
                case LAST_NAME:
                    tilLast.setError(getString(R.string.register_last_name_error));
                    break;
                case EMAIL:
                    tilEmail.setError(getString(R.string.register_email_error));
                    break;
                case COUNTRY:
                    tilCountry.setError(getString(R.string.register_country_error));
                    break;
                case BIRTHDAY:
                    tilBirthday.setError(getString(R.string.register_birthday_error));
                    break;
                case PASSWORD:
                    tilPassword.setError(getString(R.string.register_password_error));
                    break;
            }
        }
    }

    @Override
    public void hideAllErrors() {
        tilFirst.setError(null);
        tilLast.setError(null);
        tilEmail.setError(null);
        tilCountry.setError(null);
        tilBirthday.setError(null);
        tilPassword.setError(null);
    }

    public void showDatePickerDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar birthdayCal = Calendar.getInstance();
                birthdayCal.set(Calendar.YEAR, year);
                birthdayCal.set(Calendar.MONTH, monthOfYear);
                birthdayCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etBirthday.setText(Formatter.formatDateString(new Date(birthdayCal.getTimeInMillis())));
            }
        };

        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpDialog.show();
    }
}
