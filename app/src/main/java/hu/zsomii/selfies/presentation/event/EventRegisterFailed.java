package hu.zsomii.selfies.presentation.event;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class EventRegisterFailed {
    String errorMessage;


    public EventRegisterFailed(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
