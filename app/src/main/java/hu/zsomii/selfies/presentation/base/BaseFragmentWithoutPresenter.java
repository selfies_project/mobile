package hu.zsomii.selfies.presentation.base;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import hu.zsomii.selfies.R;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class BaseFragmentWithoutPresenter extends Fragment implements IBaseScreen {

    ProgressDialog dialog;


    @Override
    public void navigateTo(Class<?> target) {

    }

    @Override
    public void navigateWithFinish(Class<?> target) {

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void finish() {

    }

    @Override
    public void showLoader(){
        dialog = new ProgressDialog(getContext());
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getString(R.string.loading_text));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    @Override
    public void hideLoader(){
        if (dialog != null){
            dialog.hide();
        }
    }

}
