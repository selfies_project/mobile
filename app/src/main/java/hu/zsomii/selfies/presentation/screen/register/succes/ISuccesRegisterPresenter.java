package hu.zsomii.selfies.presentation.screen.register.succes;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/8/2018.
 */

public interface ISuccesRegisterPresenter extends IBasePresenter {
    void goToLoginPressed();
}
