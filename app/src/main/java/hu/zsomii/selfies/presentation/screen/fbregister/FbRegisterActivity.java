package hu.zsomii.selfies.presentation.screen.fbregister;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.presentation.screen.register.RegisterActivity;

public class FbRegisterActivity extends BaseActivity implements IFbRegisterActivity {

    @Inject
    IFbRegisterPresenter presenter;

    private CallbackManager callbackManager;
    private LoginButton loginButton;

    String PUBLIC_PROFILE = "public_profile";
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String EMAIL = "email";

    String email;
    String firstName;
    String lastName;
    String id;

    public FbRegisterActivity() {
        Selfies.injector().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fb_register);


        loginButton = findViewById(R.id.fbLoginButton);
        loginButton.setReadPermissions(Arrays.asList(PUBLIC_PROFILE, EMAIL));

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        setFacebookData(loginResult);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });
    }

    private void setFacebookData(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            email = response.getJSONObject().getString("email");
                            firstName = response.getJSONObject().getString("first_name");
                            lastName = response.getJSONObject().getString("last_name");
                            id = response.getJSONObject().getString("id");

                            presenter.checkFbToken(response.getJSONObject().getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void navigateToRegister() {
        Intent startRegIntent = new Intent(FbRegisterActivity.this, RegisterActivity.class);
        startRegIntent.putExtra(Globals.FB_REG_KEY_EMAIL, email);
        startRegIntent.putExtra(Globals.FB_REG_KEY_FIRST, firstName);
        startRegIntent.putExtra(Globals.FB_REG_KEY_LAST, lastName);
        startRegIntent.putExtra(Globals.FB_REG_KEY_TOKEN, id);

        startActivity(startRegIntent);
    }
}
