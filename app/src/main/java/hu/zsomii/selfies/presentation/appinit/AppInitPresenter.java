package hu.zsomii.selfies.presentation.appinit;

import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.appinit.IAppInitInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventAppInitFailed;
import hu.zsomii.selfies.presentation.event.EventInitialDataStored;
import hu.zsomii.selfies.presentation.screen.dashboard.DashboardActivity;

/**
 * Created by Adam Varga on 4/7/2018.
 */

public class AppInitPresenter extends BasePresenter implements IAppInitPresenter {

    @Inject
    ISessionInteractor sessionInteractor;
    @Inject
    IAppInitInteractor appInitInteractor;
    @Inject
    Context context;

    public AppInitPresenter() {
        Selfies.injector().inject(this);
    }

    public void onEvent(EventAppInitFailed event) {
        screen.showToast(context.getString(R.string.app_init_error));
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
    }

}
