package hu.zsomii.selfies.presentation.screen.editprofile;

import android.graphics.Bitmap;

import java.util.ArrayList;

import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.stub.ModifyUserDto;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public interface IEditProfileActivity {
    void showUserData(User user);
    ModifyUserDto getAllData();
    void showErrors(ArrayList<ProfileFields> fields);
    void hideAllErrors();
    void showImagePicker();
    Bitmap getTakenPicture();
}
