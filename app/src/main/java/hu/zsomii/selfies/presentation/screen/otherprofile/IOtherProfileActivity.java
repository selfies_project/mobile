package hu.zsomii.selfies.presentation.screen.otherprofile;

import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.stub.ProfileStat;
import hu.zsomii.selfies.stub.User;

public interface IOtherProfileActivity extends IBaseScreen {
    void showProfileStat(ProfileStat profileStat);
    void showUserData(User otherUser);
    void goToAlbum();
}
