package hu.zsomii.selfies.presentation.screen.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public class LoginActivity extends BaseActivity implements ILoginActivity {

    @Inject
    ILoginPresenter presenter;

    TextInputEditText etEmail;
    TextInputEditText etPassword;
    TextInputLayout tilPassword;
    TextInputLayout tilEmail;
    CheckBox cbStoreCredentials;
    Button btnLogin;
    Button btnTerms;
    RelativeLayout rlParent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Selfies.injector().inject(this);
        initViews();
        initInputLayouts();
    }

    @SuppressLint("CutPasteId")
    private void initViews() {
        tilEmail = findViewById(R.id.ifEmail).findViewById(R.id.textInputLayout);
        etEmail = findViewById(R.id.ifEmail).findViewById(R.id.textInputEditText);
        tilPassword = findViewById(R.id.ifPassword).findViewById(R.id.textInputLayout);
        etPassword = findViewById(R.id.ifPassword).findViewById(R.id.textInputEditText);
        cbStoreCredentials = findViewById(R.id.cbStoreCredentials);
        btnLogin = findViewById(R.id.btnLogin);
        btnTerms = findViewById(R.id.btnTerms);
        rlParent = findViewById(R.id.rlParent);

        rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.loginPressed();
            }
        });

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.termsPressed();
            }
        });
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    private void initInputLayouts() {
        tilEmail.setHint(getString(R.string.register_email));
        tilPassword.setHint(getString(R.string.register_password));
    }

    @Override
    public String getEmail() {
        return etEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return etPassword.getText().toString();
    }

    @Override
    public void showEmailError(String errorText) {
        tilEmail.setError(errorText);
    }

    @Override
    public void showPasswordError(String errorText) {
        tilPassword.setError(errorText);
    }

    @Override
    public void hideEmailError() {
        tilEmail.setError(null);
    }

    @Override
    public void hidePasswordError() {
        tilPassword.setError(null);
    }

    @Override
    public boolean shouldStoreCredentials() {
        return cbStoreCredentials.isChecked();
    }

    @Override
    public void showPreviousCredentials(String email, String password) {
        etEmail.setText(email);
        etPassword.setText(password);

        if (!email.isEmpty())
            cbStoreCredentials.setChecked(true);

    }
}
