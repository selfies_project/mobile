package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.AvailableLocation;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class EventLocationsDownloaded {
    private ArrayList<AvailableLocation> locations;


    public ArrayList<AvailableLocation> getAvailableLocations() {
        return locations;
    }


    public EventLocationsDownloaded(ArrayList<AvailableLocation> locations) {
        this.locations = locations;

    }
}
