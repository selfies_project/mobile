package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.AvailableLocation;

/**
 * Created by Adam Varga on 4/8/2018.
 */

public class EventAppInitLocationsObtained {
    private ArrayList<AvailableLocation> locations;


    public ArrayList<AvailableLocation> getAvailableLocations() {
        return locations;
    }


    public EventAppInitLocationsObtained(ArrayList<AvailableLocation> locations) {
        this.locations = locations;
    }

}
