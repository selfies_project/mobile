package hu.zsomii.selfies.presentation.screen.otherprofilealbum;

import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.stub.ReportDto;

public interface IOtherProfileAlbumPresenter extends IBasePresenter {
    void showImageClicked(String id);
    void reportClicked();
    void sendReportClicked(ReportDto reportDto);
}
