package hu.zsomii.selfies.presentation.appinit;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 4/7/2018.
 */

public interface IAppInitPresenter extends IBasePresenter {
}
