package hu.zsomii.selfies.presentation.screen.editprofile;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public interface IEditProfilePresenter extends IBasePresenter {
    void selectProfilePicturePressed();
    void savePressed(boolean imageSelected);
}
