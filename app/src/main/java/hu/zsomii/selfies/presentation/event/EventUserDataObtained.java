package hu.zsomii.selfies.presentation.event;

import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/16/2018.
 */

public class EventUserDataObtained {

    private User userData;

    public User getUserData() {
        return userData;
    }

    public EventUserDataObtained(User userData) {
        this.userData = userData;
    }
}
