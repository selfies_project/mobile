package hu.zsomii.selfies.presentation.screen.splash;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import io.fabric.sdk.android.Fabric;

public class SplashScreenActivity extends BaseActivity implements ISplashScreenActivity {

    Button btnLogin;
    Button btnRegister;
    Button btnFBLogin;
    Button btnTerms;
    Button btnExternalLogin;
    CallbackManager callbackManager;
    PopupWindow oldVersionPopup;
    RelativeLayout rlSplash;

    @Inject
    ISplashScreenPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        rlSplash = findViewById(R.id.rlSplash);

        Selfies.injector().inject(this);
        logKeyHash();
        initButtons();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }



    private void initButtons() {
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);
        btnTerms = findViewById(R.id.btnTerms);
        btnFBLogin = findViewById(R.id.btnFBLogin);
        btnExternalLogin = findViewById(R.id.btnExternalLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.loginPressed();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.registerPressed();
            }
        });

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.termsPressed();
            }
        });

        btnFBLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.fbRegisterPressed();
            }
        });

    }

    private void logKeyHash() {
        try {

            @SuppressLint("PackageManagerGetSignatures") PackageInfo info =
                    getPackageManager().getPackageInfo("hu.zsomii.selfies",
                            PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showOldVersionPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_old_version);

        Button dialogButton =  dialog.findViewById(R.id.btnNavigate);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.facebook.katana"));
                startActivity(intent);
            }
        });

        dialog.show();
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

}
