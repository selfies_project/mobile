package hu.zsomii.selfies.presentation.screen.terms;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class TermsActivity extends BaseActivity implements ITermsActivity {

    @Inject
    ITermsPresenter presenter;

    TextView tvTerms;
    Button btnClose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Selfies.injector().inject(this);

        setContentView(R.layout.activity_terms);

        tvTerms = findViewById(R.id.tvTerms);
        btnClose = findViewById(R.id.btnClose);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.closePressed();
            }
        });
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }
}
