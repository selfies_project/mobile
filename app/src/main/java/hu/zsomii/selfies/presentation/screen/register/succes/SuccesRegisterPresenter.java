package hu.zsomii.selfies.presentation.screen.register.succes;

import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.screen.login.LoginActivity;

/**
 * Created by Adam Varga on 3/8/2018.
 */

public class SuccesRegisterPresenter extends BasePresenter implements ISuccesRegisterPresenter  {


    @Override
    public void goToLoginPressed() {
        screen.navigateWithFinish(LoginActivity.class);
    }
}
