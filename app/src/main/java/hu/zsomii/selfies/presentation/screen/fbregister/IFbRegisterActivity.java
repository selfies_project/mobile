package hu.zsomii.selfies.presentation.screen.fbregister;

import hu.zsomii.selfies.presentation.base.IBaseScreen;

public interface IFbRegisterActivity extends IBaseScreen {
    void navigateToRegister();
}
