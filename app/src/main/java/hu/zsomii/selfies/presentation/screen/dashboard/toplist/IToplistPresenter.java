package hu.zsomii.selfies.presentation.screen.dashboard.toplist;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public interface IToplistPresenter extends IBasePresenter {
    void refreshTopList();
    void userClicked(String id);
    String getCurrentUserId();
}
