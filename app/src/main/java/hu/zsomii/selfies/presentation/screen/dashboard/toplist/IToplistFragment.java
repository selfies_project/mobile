package hu.zsomii.selfies.presentation.screen.dashboard.toplist;

import java.util.ArrayList;

import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.stub.ToplistUserDto;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public interface IToplistFragment extends IBaseScreen {
    void populateToplist(ArrayList<ToplistUserDto> toplistUsers);
    void goToOtherProfile();
}
