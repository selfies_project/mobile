package hu.zsomii.selfies.presentation.screen.otherprofilealbum;

import android.content.Context;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.profile.IProfileInteractor;
import hu.zsomii.selfies.interactor.profile.event.EventCurrentUserImagesObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherUserImageDetailObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherUserImagesObtained;
import hu.zsomii.selfies.interactor.profile.event.EventReportSuccess;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.stub.ReportDto;

public class OtherProfileAlbumPresenter extends BasePresenter implements IOtherProfileAlbumPresenter {

    @Inject
    IProfileInteractor profileInteractor;

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    Context context;

    public OtherProfileAlbumPresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        screen.showLoader();
        profileInteractor.getImagesForUser();
    }

    public void onEvent(EventOtherUserImagesObtained event) {
        if (screen != null) {
            screen.hideLoader();
            ((IOtherProfileAlbumActivity) screen).showAlbum(event.getImages());
        }
    }

    public void onEvent(EventCurrentUserImagesObtained event) {
        if (screen != null) {
            screen.hideLoader();
            ((IOtherProfileAlbumActivity) screen).showAlbum(event.getImages());
        }
    }

    @Override
    public void showImageClicked(String id) {
        screen.showLoader();
        sessionInteractor.setSelectedImage(id);
        profileInteractor.getImageDetails();
    }

    @Override
    public void reportClicked() {
        ((IOtherProfileAlbumActivity)screen).showReportPopup();
    }

    @Override
    public void sendReportClicked(ReportDto reportDto) {
        screen.showLoader();
        profileInteractor.sendReport(reportDto);
    }

    public void onEvent(EventOtherUserImageDetailObtained event) {
        if (screen != null) {
            screen.hideLoader();
            ((IOtherProfileAlbumActivity) screen).showImage(event.getImageDetail(), sessionInteractor.getSelectedUserId() != null);
        }
    }

    public void onEvent(EventReportSuccess event) {
        if (screen != null) {
            screen.hideLoader();
            screen.showToast(context.getString(R.string.report_succes));
        }
    }
}
