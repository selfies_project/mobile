package hu.zsomii.selfies.presentation.screen.login;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface ILoginPresenter extends IBasePresenter {
    void loginPressed();
    void termsPressed();
}
