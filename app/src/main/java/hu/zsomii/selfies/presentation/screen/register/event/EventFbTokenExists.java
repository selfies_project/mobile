package hu.zsomii.selfies.presentation.screen.register.event;

import hu.zsomii.selfies.stub.LoginUserDto;
import hu.zsomii.selfies.stub.UserDto;

public class EventFbTokenExists {

  private String userToken;

    public EventFbTokenExists(String userToken) {
        this.userToken = userToken;
    }

    public String getUser() {
        return userToken;
    }

    public void setUser(String user) {
        this.userToken = user;
    }
}
