package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.Selfie;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class EventDiscoverItemsDownloaded {
    private ArrayList<Selfie> selfies;

    public ArrayList<Selfie> getSelfies() {
        return selfies;
    }

    public EventDiscoverItemsDownloaded(ArrayList<Selfie> selfies) {
        this.selfies = selfies;
    }
}
