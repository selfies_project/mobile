package hu.zsomii.selfies.presentation.screen.editprofile;

import android.content.Context;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.editprofile.IEditProfileInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventModifyValidationError;
import hu.zsomii.selfies.presentation.event.EventProfileModificationFailed;
import hu.zsomii.selfies.presentation.event.EventProfileModificationSucces;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public class EditProfilePresenter extends BasePresenter implements IEditProfilePresenter {

    @Inject
    IEditProfileInteractor interactor;

    @Inject
    Context context;

    public EditProfilePresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        ((IEditProfileActivity) screen).showUserData(interactor.currentUserData());
    }

    @Override
    public void selectProfilePicturePressed() {
        ((IEditProfileActivity) screen).showImagePicker();
    }

    public void onEvent(EventProfileModificationSucces event) {
        if (screen != null) {
            screen.hideLoader();
            screen.finish();
        }
    }

    public void onEvent(EventModifyValidationError event) {
        if (screen != null) {
            screen.hideLoader();
        }
        ((IEditProfileActivity) screen).showErrors(event.getFields());
    }

    public void onEvent(EventProfileModificationFailed event) {
        if (screen != null) {
            screen.hideLoader();
            screen.showToast(context.getString(R.string.edit_profile_invalid_mod));
        }
    }

    @Override
    public void savePressed(boolean imageSelected) {
        if (screen != null) {
            screen.showLoader();
            if (imageSelected) {
                interactor.modifyUserData(((IEditProfileActivity) screen).getTakenPicture(), ((IEditProfileActivity) screen).getAllData());
            } else interactor.modifyUserData(null, ((IEditProfileActivity) screen).getAllData());

        }
    }
}
