package hu.zsomii.selfies.presentation.screen.dashboard.share;

import java.util.ArrayList;

import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.stub.AvailableLocation;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public interface IShareFragment extends IBaseScreen {
    void showAvailableLocations(ArrayList<AvailableLocation> locations);
    void showMap(float lat, float lng, String name);
    void startPictureTakingActivity();
}
