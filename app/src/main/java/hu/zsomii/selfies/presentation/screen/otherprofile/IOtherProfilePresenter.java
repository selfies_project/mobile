package hu.zsomii.selfies.presentation.screen.otherprofile;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

public interface IOtherProfilePresenter extends IBasePresenter {
    void showAlbumClicked();
}
