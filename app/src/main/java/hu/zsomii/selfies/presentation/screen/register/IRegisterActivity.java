package hu.zsomii.selfies.presentation.screen.register;

import android.graphics.Bitmap;

import java.util.ArrayList;

import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public interface IRegisterActivity {
    User getAllFieldsContent();
    void takePicture();
    Bitmap getTakenPicture();
    void showErrors(ArrayList<ProfileFields> fields);
    void hideAllErrors();
}
