package hu.zsomii.selfies.presentation.appinit;

import hu.zsomii.selfies.presentation.base.IBaseScreen;

/**
 * Created by Adam Varga on 4/7/2018.
 */

public interface IAppInitActivity extends IBaseScreen {
}
