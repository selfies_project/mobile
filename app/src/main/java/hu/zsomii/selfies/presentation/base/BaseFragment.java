package hu.zsomii.selfies.presentation.base;


import android.app.ProgressDialog;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public abstract class BaseFragment extends BaseFragmentWithoutPresenter {

    ProgressDialog dialog;

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().attachScreen(this);
    }

    @Override
    public void onPause() {
        getPresenter().detachScreen();
        super.onPause();
    }




    protected abstract IBasePresenter getPresenter();
}
