package hu.zsomii.selfies.presentation.screen.dashboard.share;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseFragment;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.TakePictureActivity;
import hu.zsomii.selfies.stub.AvailableLocation;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public class ShareFragment extends BaseFragment implements IShareFragment {

    @Inject
    ISharePresenter presenter;

    RecyclerView rvGoalList;

    public ShareFragment() {
        Selfies.injector().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_goallist, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvGoalList = view.findViewById(R.id.rvGoalList);
        presenter.refreshAvailableLocation();
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }


    @Override
    public void showAvailableLocations(ArrayList<AvailableLocation> locations) {
        ShareAdapter adapter = new ShareAdapter(locations, getContext(), presenter);
        rvGoalList.setAdapter(adapter);
        rvGoalList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void showMap(final float lat, final float lng, final String name) {
        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_google_map);
        MapView mMapView = dialog.findViewById(R.id.mapView);
        MapsInitializer.initialize(getActivity());

        mMapView.onCreate(dialog.onSaveInstanceState());
        mMapView.onResume();


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                LatLng location = new LatLng(lat, lng);
                googleMap.addMarker(new MarkerOptions().position(location).title(name));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
            }
        });


        dialog.show();
    }

    @Override
    public void startPictureTakingActivity() {
        startActivity(new Intent(getContext(), TakePictureActivity.class));
    }
}
