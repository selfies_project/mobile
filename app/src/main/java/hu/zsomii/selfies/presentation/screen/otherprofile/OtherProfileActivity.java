package hu.zsomii.selfies.presentation.screen.otherprofile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.OtherProfileAlbumActivity;
import hu.zsomii.selfies.stub.ProfileStat;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.utils.GlideImageView;

public class OtherProfileActivity extends BaseActivity implements IOtherProfileActivity {

    @Inject
    IOtherProfilePresenter presenter;

    CircleImageView civProfileImage;
    ImageView ivHeaderBackground;
    ImageView ivSelfiesIcon;
    ImageView ivScoreIcon;
    ImageView ivLikesIcon;
    TextView tvSelfies;
    TextView tvScore;
    TextView tvLikes;
    TextView tvName;
    TextView tvBirthday;
    TextView tvCountry;
    TextView tvEmail;
    Formatter formatter;

    public OtherProfileActivity() {
        Selfies.injector().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile);

        initComponents();
        formatter = new Formatter();
    }

    @SuppressLint({"SetTextI18n", "CutPasteId"})
    private void initComponents() {
        civProfileImage = findViewById(R.id.civProfileImage);
        ivHeaderBackground = findViewById(R.id.ivHeaderBackground);

        tvName = findViewById(R.id.tvName);
        tvBirthday = findViewById(R.id.tvBirthday);
        tvCountry = findViewById(R.id.tvCountry);
        tvEmail = findViewById(R.id.tvEmail);

        ivSelfiesIcon = findViewById(R.id.headerItemImages).findViewById(R.id.ivItemIcon);
        ivScoreIcon = findViewById(R.id.headerItemScore).findViewById(R.id.ivItemIcon);
        ivLikesIcon = findViewById(R.id.headerItemLikes).findViewById(R.id.ivItemIcon);

        tvSelfies = findViewById(R.id.headerItemImages).findViewById(R.id.tvCount);
        tvScore = findViewById(R.id.headerItemScore).findViewById(R.id.tvCount);
        tvLikes = findViewById(R.id.headerItemLikes).findViewById(R.id.tvCount);
    }

    public void setProfileStats(int selfies, int score, int likes) {
        ivSelfiesIcon.setImageResource(R.drawable.camera);
        ivScoreIcon.setImageResource(R.drawable.star);
        ivLikesIcon.setImageResource(R.drawable.heart);

        tvSelfies.setText(String.valueOf(selfies));
        tvScore.setText(String.valueOf(score));
        tvLikes.setText(String.valueOf(likes));
    }

    @Override
    public void showProfileStat(ProfileStat profileStat) {
        setProfileStats(profileStat.getSelfieCount(), profileStat.getScore(), profileStat.getLikeCount());

        if (profileStat.getSelfieCount() > 0 ){
            findViewById(R.id.headerItemImages).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.showAlbumClicked();
                }
            });
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void showUserData(User userData) {
        tvName.setText(userData.getFirstName() + " " + userData.getLastName());
        tvBirthday.setText(userData.getBirthDate());
        tvBirthday.setText(Formatter.formatBirthDayDate(Formatter.formatDate(userData.getBirthDate())));
        tvCountry.setText(userData.getCountry());
        tvEmail.setText(userData.getEmail());

        GlideImageView.loadImageById(this, userData.getProfilePictureJson(), civProfileImage);
        GlideImageView.loadBlurImageById(this, userData.getProfilePictureJson(), ivHeaderBackground, 50);
    }

    @Override
    public void goToAlbum() {
        startActivity(new Intent(this, OtherProfileAlbumActivity.class));
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

}
