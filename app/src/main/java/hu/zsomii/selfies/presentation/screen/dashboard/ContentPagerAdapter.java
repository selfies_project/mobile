package hu.zsomii.selfies.presentation.screen.dashboard;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.DiscoverListFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.ProfileFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.share.ShareFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.ToplistFragment;

/**
 * Created by Adam Varga on 2/18/2018.
 */


public class ContentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    ContentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new ProfileFragment();
        } else if (position == 1) {
            return new DiscoverListFragment();
        } else if (position == 2) {
            return new ShareFragment();
        } else {
            return new ToplistFragment();
        }
    }


    @Override
    public int getCount() {
        return 4;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.dashboard_title_profile);
            case 1:
                return mContext.getString(R.string.dashboard_title_discover);
            case 2:
                return mContext.getString(R.string.dashboard_title_share);
            case 3:
                return mContext.getString(R.string.dashboard_title_toplist);
            default:
                return null;
        }
    }

}