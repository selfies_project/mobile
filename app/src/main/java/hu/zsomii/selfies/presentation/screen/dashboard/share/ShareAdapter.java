package hu.zsomii.selfies.presentation.screen.dashboard.share;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.stub.AvailableLocation;
import hu.zsomii.selfies.utils.GlideImageView;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.MyViewHolder> {

    private List<AvailableLocation> availableLocations;
    private Context ctx;
    private ImageView ivGoalListBackground;
    private ImageView ivShowLocation;
    private ImageView ivTakePicture;
    private TextView tvGoalName;
    private TextView tvGoalDistance;
    private TextView tvGoalScore;
    private TextView tvGoalValid;
    private ISharePresenter presenter;

    class MyViewHolder extends RecyclerView.ViewHolder {
        MyViewHolder(View view) {
            super(view);

            ivGoalListBackground = view.findViewById(R.id.ivGoalListBackground);
            tvGoalName = view.findViewById(R.id.tvGoalName);
            tvGoalDistance = view.findViewById(R.id.tvGoalDistance);
            tvGoalScore = view.findViewById(R.id.tvGoalScore);
            tvGoalValid = view.findViewById(R.id.tvGoalValid);
            ivShowLocation = view.findViewById(R.id.ivShowLocation);
            ivTakePicture = view.findViewById(R.id.ivTakePicture);
        }
    }


    ShareAdapter(List<AvailableLocation> availableLocations, Context ctx, ISharePresenter presenter) {
        this.availableLocations = availableLocations;
        this.ctx = ctx;
        this.presenter = presenter;
    }

    @Override
    public ShareAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_goallist_item, parent, false);

        return new ShareAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(ShareAdapter.MyViewHolder holder, int position) {
       final AvailableLocation item = availableLocations.get(position);

        GlideImageView.loadImageById(ctx, item.getImageId(), ivGoalListBackground);

        tvGoalName.setText(item.getName());
        tvGoalDistance.setText(Formatter.getDistanceString(item.getDistance()));
        tvGoalScore.setText(String.valueOf(item.getValue()));
        tvGoalValid.setText(Formatter.truncateDateString(item.getDeadline()));

        if (item.isAlreadyUsed()) {
            makeImageDisabled();
        } else {
            resetImage();

            ivShowLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onShowLocationClicked(item.getLatitude(), item.getLongitude(), item.getName());
                }
            });

            ivTakePicture.setVisibility((item.getDistance() <= Globals.GEOFENCE_RADIUS_IN_METERS) ? View.VISIBLE : View.GONE);
            ivTakePicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onTakePictureClicked(item.getId());
                }
            });
        }
    }

    private void makeImageDisabled() {
        ivShowLocation.setVisibility(View.GONE);
        ivTakePicture.setVisibility(View.GONE);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        ivGoalListBackground.setColorFilter(cf);
        ivGoalListBackground.setImageAlpha(128);
    }

    private void resetImage() {
        ivGoalListBackground.setColorFilter(null);
        ivGoalListBackground.setImageAlpha(255);

        ivShowLocation.setVisibility(View.VISIBLE);
        ivTakePicture.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return availableLocations.size();
    }
}
