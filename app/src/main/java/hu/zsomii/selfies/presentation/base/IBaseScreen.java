package hu.zsomii.selfies.presentation.base;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public interface IBaseScreen {
    void navigateTo(Class<?> target);
    void navigateWithFinish(Class<?> target);
    void showToast(String message);
    void finish();
    void showLoader();
    void hideLoader();
}
