package hu.zsomii.selfies.presentation.event;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public class EventModificationFailed {
    String message;

    public EventModificationFailed(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
