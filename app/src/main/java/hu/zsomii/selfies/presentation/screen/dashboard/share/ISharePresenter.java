package hu.zsomii.selfies.presentation.screen.dashboard.share;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public interface ISharePresenter  extends IBasePresenter {
    void refreshAvailableLocation();
    void onShowLocationClicked(float lat, float lng, String name);
    void onTakePictureClicked(String locationId);
}
