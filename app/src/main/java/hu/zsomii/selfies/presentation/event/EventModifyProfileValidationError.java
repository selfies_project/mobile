package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.interactor.register.ProfileFields;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public class EventModifyProfileValidationError {
    ArrayList<ProfileFields> fields;

    public EventModifyProfileValidationError(ArrayList<ProfileFields> fields) {
        this.fields = fields;
    }

    public ArrayList<ProfileFields> getFields() {
        return fields;
    }
}
