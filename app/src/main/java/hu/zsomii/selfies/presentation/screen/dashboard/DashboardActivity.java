package hu.zsomii.selfies.presentation.screen.dashboard;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.JsonReader;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.services.GeofenceTransitionsIntentService;
import hu.zsomii.selfies.stub.AvailableLocation;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public class DashboardActivity extends BaseActivity implements IDashboardActivity, LocationListener {

    LocationManager locationManager;
    String provider;

    private GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent;

    @Inject
    IDashboardPresenter presenter;

    @Inject
    ISharedPref sharedPref;

    @Inject
    ModelMapper mapper;

    public static final int LOCATION_PERMISSION = 99;
    public static final int CAMERA_PERMISSION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Selfies.injector().inject(this);
        setContentView(R.layout.activity_dashboard);
        ViewPager viewPager = findViewById(R.id.vpContent);

        ContentPagerAdapter adapter = new ContentPagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tlContent);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(
                getResources().getColor(R.color.white),
                getResources().getColor(R.color.white)
        );
        tabLayout.setBackgroundResource(R.color.vogue_red);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        askForLocationPermission();
        askForCameraPermission();

        mGeofencingClient = LocationServices.getGeofencingClient(this);
    }


    public void askForLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.permission_location_title)
                        .setMessage(R.string.permission_location_text)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(DashboardActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        LOCATION_PERMISSION);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION);
            }
        }
    }

    public void askForCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.permission_camera_title)
                        .setMessage(R.string.permission_camera_text)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        provider = locationManager.getBestProvider(new Criteria(), false);
        switch (requestCode) {
            case LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                }
            }
            case CAMERA_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.permissionGranted(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
                } else
                    presenter.permissionGranted(false);
            }
        }

        hideLoader();
    }


    public void addGeofences(ArrayList<AvailableLocation> locations) {
        hideLoader();
        ArrayList<Geofence> mGeofenceList = new ArrayList<>();
        String alreadyAddedGeofenceIdsString = sharedPref.getString(Globals.GEOFENCE_ALREADY_ADDED_IDS);
        final ArrayList<String> alreadyAddedGeofenceIds;

        if (alreadyAddedGeofenceIdsString == null || alreadyAddedGeofenceIdsString.isEmpty()) {
            alreadyAddedGeofenceIds = new ArrayList<>();
        } else {
            java.lang.reflect.Type targetListType = new TypeToken<ArrayList<String>>() {}.getType();
            alreadyAddedGeofenceIds = mapper.map(alreadyAddedGeofenceIdsString, targetListType);
        }

        for (AvailableLocation loc : locations) {

            if (!alreadyAddedGeofenceIds.contains(loc.getId())) {
                mGeofenceList.add(new Geofence.Builder()
                        .setRequestId(loc.getId())
                        .setCircularRegion(
                                loc.getLatitude(),
                                loc.getLongitude(),
                                Globals.GEOFENCE_RADIUS_IN_METERS
                        )
                        .setExpirationDuration(countTimeLeftForGeofencePoint(loc.getDeadline()))
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build());

                alreadyAddedGeofenceIds.add(loc.getId());
            }


        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            showToast(getString(R.string.no_location_provided));
        } else {
            mGeofencingClient.addGeofences(getGeofencingRequest(mGeofenceList), getGeofencePendingIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            ModelMapper mapper = new ModelMapper();
                            sharedPref.storeString(Globals.GEOFENCE_ALREADY_ADDED_IDS, mapper.map(alreadyAddedGeofenceIds, String.class));
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            showToast(getString(R.string.no_location_provided));
                        }
                    });

        }

    }

    private long countTimeLeftForGeofencePoint(String deadline) {
        Date temp = Formatter.formatDate(deadline);
        Date currentDate = Calendar.getInstance().getTime();

        if (currentDate.getTime() > temp.getTime()) {
            return 0;
        }

        return temp.getTime() - currentDate.getTime();
    }

    private GeofencingRequest getGeofencingRequest(List<Geofence> mGeofenceList) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }


    @Override
    public void onBackPressed() {

    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

}



