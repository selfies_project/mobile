package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.interactor.register.ProfileFields;

/**
 * Created by Adam Varga on 3/9/2018.
 */

public class EventRegisterValidationError {
    private ArrayList<ProfileFields> fields;

    public EventRegisterValidationError(ArrayList<ProfileFields> fields) {
        this.fields = fields;
    }

    public ArrayList<ProfileFields> getFields() {
        return fields;
    }
}
