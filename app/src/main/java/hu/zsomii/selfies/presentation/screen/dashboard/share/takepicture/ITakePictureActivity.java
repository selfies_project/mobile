package hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture;

import android.graphics.Bitmap;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public interface ITakePictureActivity {
    Bitmap getTakenPicture();
    void finish();
}
