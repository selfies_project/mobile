package hu.zsomii.selfies.presentation.screen.dashboard.profile;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public interface IProfilePresenter extends IBasePresenter {
    void editButtonPressed();
    void showAlbumClicked();
}
