package hu.zsomii.selfies.presentation.screen.otherprofilealbum;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.stub.ImageDetailDto;
import hu.zsomii.selfies.stub.ReportDto;
import hu.zsomii.selfies.utils.GlideImageView;


public class OtherProfileAlbumActivity extends BaseActivity implements IOtherProfileAlbumActivity {

    @Inject
    IOtherProfileAlbumPresenter presenter;

    RecyclerView rvAlbum;
    ImageView ivReport;
    ImageView ivImage;
    TextView tvGoal;
    TextView tvDate;

    Button btnSendReport;
    EditText etDetail;
    Spinner spReason;

    public OtherProfileAlbumActivity() {
        Selfies.injector().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile_album);

        rvAlbum = findViewById(R.id.rvAlbum);
    }

    @Override
    public void showAlbum(ArrayList<String> images) {
        OtherProfileAlbumAdapter adapter = new OtherProfileAlbumAdapter(images, this, presenter);
        rvAlbum.setAdapter(adapter);
        rvAlbum.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
    }

    @Override
    public void showImage(ImageDetailDto imageDetail, boolean canReport) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_image_detail);

        ivReport = dialog.findViewById(R.id.ivReport);
        ivImage = dialog.findViewById(R.id.ivImage);
        tvGoal = dialog.findViewById(R.id.tvGoal);
        tvDate = dialog.findViewById(R.id.tvDate);

        tvGoal.setText(imageDetail.getLocationName());
        tvDate.setText(Formatter.formatDayDate(imageDetail.getUploadDate()));
        GlideImageView.loadImageById(this, imageDetail.getImageId(), ivImage);

        if (canReport) {
            ivReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.reportClicked();
                    dialog.dismiss();
                }
            });
        }
        else{
            ivReport.setVisibility(View.GONE);
        }
        dialog.show();
    }

    @Override
    public void showReportPopup() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_report);

        spReason = dialog.findViewById(R.id.spReason);
        etDetail = dialog.findViewById(R.id.etDetail);
        btnSendReport = dialog.findViewById(R.id.btnSendReport);

        btnSendReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReportDto reportDto = new ReportDto();
                reportDto.setReportType(spReason.getSelectedItem().toString());
                reportDto.setReason(etDetail.getText().toString());

                presenter.sendReportClicked(reportDto);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }
}

