package hu.zsomii.selfies.presentation.screen.otherprofilealbum;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.utils.GlideImageView;


public class OtherProfileAlbumAdapter extends RecyclerView.Adapter<OtherProfileAlbumAdapter.MyViewHolder> {

    private ImageView ivContent;
    private Context ctx;
    private ArrayList<String> imagesList;
    private IOtherProfileAlbumPresenter presenter;

    class MyViewHolder extends RecyclerView.ViewHolder {

        MyViewHolder(View view) {
            super(view);

            ivContent = view.findViewById(R.id.ivContent);
        }
    }

    OtherProfileAlbumAdapter(ArrayList<String> imagesList, Context ctx, IOtherProfileAlbumPresenter presenter) {
        this.imagesList = imagesList;
        this.ctx = ctx;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public OtherProfileAlbumAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_album_item, parent, false);

        return new OtherProfileAlbumAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OtherProfileAlbumAdapter.MyViewHolder holder, int position) {
        final String id = imagesList.get(position);

        GlideImageView.loadImageById(ctx, id, ivContent);
        ivContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showImageClicked(id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

}