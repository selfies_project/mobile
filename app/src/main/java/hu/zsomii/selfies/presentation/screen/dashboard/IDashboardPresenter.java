package hu.zsomii.selfies.presentation.screen.dashboard;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface IDashboardPresenter extends IBasePresenter {
    void permissionGranted(boolean b);
}
