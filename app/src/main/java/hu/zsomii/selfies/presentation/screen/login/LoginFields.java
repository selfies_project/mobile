package hu.zsomii.selfies.presentation.screen.login;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public enum LoginFields {
    EMAIL, PASSWORD
}
