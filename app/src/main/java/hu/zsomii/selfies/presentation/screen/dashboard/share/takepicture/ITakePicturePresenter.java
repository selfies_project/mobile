package hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture;

import android.graphics.Bitmap;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public interface ITakePicturePresenter extends IBasePresenter {
    void uploadClicked(Bitmap file);
}
