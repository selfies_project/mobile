package hu.zsomii.selfies.presentation.screen.dashboard.discover;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.stub.Selfie;
import hu.zsomii.selfies.utils.GlideImageView;


/**
 * Created by Adam Varga on 3/8/2018.
 */

public class DiscoverAdapter extends RecyclerView.Adapter<DiscoverAdapter.MyViewHolder> {

    private List<Selfie> selfies;
    private Context ctx;
    private ImageView ivImage;
    private ImageView ivLike;
    private CircleImageView civProfile;

    private TextView tvUploaderName;
    private TextView tvUploadDate;
    private TextView tvLikes;
    private TextView tvName;
    private IDiscoverListPresenter presenter;

    class MyViewHolder extends RecyclerView.ViewHolder {
        MyViewHolder(View view) {
            super(view);
            ivImage = view.findViewById(R.id.ivImage);
            ivLike = view.findViewById(R.id.ivLike);
            civProfile = view.findViewById(R.id.civProfile);
            tvUploaderName = view.findViewById(R.id.tvUploaderName);
            tvUploaderName = view.findViewById(R.id.tvUploaderName);
            tvUploadDate = view.findViewById(R.id.tvUploadDate);
            tvName = view.findViewById(R.id.tvName);
            tvLikes = view.findViewById(R.id.tvLikes);
        }
    }

    DiscoverAdapter(List<Selfie> selfies, Context ctx, IDiscoverListPresenter presenter) {
        this.selfies = selfies;
        this.ctx = ctx;
        this.presenter = presenter;
    }

    @Override
    public DiscoverAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.component_discover_item, parent, false);
        return new DiscoverAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(DiscoverAdapter.MyViewHolder holder, int position) {
        final Selfie selfie = selfies.get(position);

        GlideImageView.loadImageById(ctx, selfie.getImageId(), ivImage);
        GlideImageView.loadImageById(ctx, selfie.getProfilePictureId(), civProfile);

        tvName.setText(selfie.getDescription());
        tvUploaderName.setText(selfie.getOwnerName());
        tvUploadDate.setText(Formatter.truncateDateString(selfie.getUploadDate()));
        tvLikes.setText(selfie.getLikeCount() + " " + ctx.getString(R.string.discover_likes));
        ivLike.setImageResource(selfie.isAlreadyLiked() ? R.drawable.icon_like_filled : R.drawable.icon_like_empty);

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.heartIconClicked(selfie.getImageId(), selfie.isAlreadyLiked());
            }
        });

    }

    @Override
    public int getItemCount() {
        return selfies.size();
    }
}