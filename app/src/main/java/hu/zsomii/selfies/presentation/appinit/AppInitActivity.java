package hu.zsomii.selfies.presentation.appinit;


import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.DashboardActivity;
import hu.zsomii.selfies.stub.AvailableLocation;

public class AppInitActivity extends BaseActivity implements IAppInitActivity {

    private static final int REQUEST_LOCATION_PERMISSION_CODE = 101;

    @Inject
    IAppInitPresenter presenter;

    public AppInitActivity() {
        Selfies.injector().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_init);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        }

        startActivity(new Intent(this, DashboardActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }


}

