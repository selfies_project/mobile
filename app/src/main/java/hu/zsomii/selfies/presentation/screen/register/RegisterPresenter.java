package hu.zsomii.selfies.presentation.screen.register;

import android.graphics.Bitmap;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.register.IRegisterInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.event.EventRegisterFailed;
import hu.zsomii.selfies.presentation.event.EventRegisterSucces;
import hu.zsomii.selfies.presentation.event.EventRegisterValidationError;
import hu.zsomii.selfies.presentation.screen.register.succes.SuccesRegisterActivity;
import hu.zsomii.selfies.presentation.screen.terms.TermsActivity;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class RegisterPresenter extends BasePresenter implements IRegisterPresenter {


    @Inject
    IRegisterInteractor interactor;

    public RegisterPresenter() {
        Selfies.injector().inject(this);
    }


    @Override
    public void registerClicked() {
        screen.showLoader();
        User user = ((IRegisterActivity) screen).getAllFieldsContent();
        Bitmap profilePicture = ((IRegisterActivity) screen).getTakenPicture();
        interactor.registerUser(user, profilePicture);

    }

    @Override
    public void chooseImageClicked() {
        ((IRegisterActivity) screen).takePicture();
    }

    public void onEvent(EventRegisterSucces event) {
        if (screen != null) {
            screen.hideLoader();
            screen.navigateWithFinish(SuccesRegisterActivity.class);
        }
    }

    public void onEvent(EventRegisterFailed event) {
        if (screen != null) {
            screen.hideLoader();
            screen.showToast(event.getErrorMessage());
        }
    }

    public void onEvent(EventRegisterValidationError event) {
        if (screen != null) {
            screen.hideLoader();
            ((IRegisterActivity)screen).hideAllErrors();
            ((IRegisterActivity)screen).showErrors(event.getFields());
        }
    }

    @Override
    public void termsClicked() {
        screen.navigateTo(TermsActivity.class);
    }

}
