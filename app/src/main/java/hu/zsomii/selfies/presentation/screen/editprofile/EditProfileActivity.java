package hu.zsomii.selfies.presentation.screen.editprofile;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.presentation.base.BaseActivity;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.stub.ModifyUserDto;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.utils.GlideImageView;

public class EditProfileActivity extends BaseActivity implements IEditProfileActivity {

    @Inject
    IEditProfilePresenter presenter;

    CircleImageView civProfileImage;
    ImageView ivHeaderBackground;
    TextInputLayout firstNameWrapper;
    TextInputLayout lastNameWrapper;
    TextInputLayout birthdateWrapper;
    TextInputLayout countryWrapper;
    TextInputLayout passwordWrapper;
    TextInputLayout oldPasswordWrapper;
    TextInputLayout emailWrapper;

    EditText fFirstName;
    EditText fLastName;
    EditText fBirthdate;
    EditText fCountry;
    EditText fPassword;
    EditText fOldPassword;
    EditText fEmail;

    Button btnSave;

    private static final int SELECT_PHOTO = 100;
    private boolean imageSelected;
    Bitmap selectedPicture;


    public EditProfileActivity() {
        Selfies.injector().inject(this);
    }

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        imageSelected = false;
        civProfileImage = findViewById(R.id.civProfileImage);
        ivHeaderBackground = findViewById(R.id.ivHeaderBackground);

        firstNameWrapper = findViewById(R.id.fFirstName).findViewById(R.id.textInputLayout);
        fFirstName = findViewById(R.id.fFirstName).findViewById(R.id.textInputEditText);

        lastNameWrapper = findViewById(R.id.fLastName).findViewById(R.id.textInputLayout);
        fLastName = findViewById(R.id.fLastName).findViewById(R.id.textInputEditText);

        birthdateWrapper = findViewById(R.id.fAge).findViewById(R.id.textInputLayout);
        fBirthdate = findViewById(R.id.fAge).findViewById(R.id.textInputEditText);

        countryWrapper = findViewById(R.id.fCountry).findViewById(R.id.textInputLayout);
        fCountry = findViewById(R.id.fCountry).findViewById(R.id.textInputEditText);

        emailWrapper = findViewById(R.id.fEmail).findViewById(R.id.textInputLayout);
        fEmail = findViewById(R.id.fEmail).findViewById(R.id.textInputEditText);

        passwordWrapper = findViewById(R.id.fPassword).findViewById(R.id.textInputLayout);
        fPassword = findViewById(R.id.fPassword).findViewById(R.id.textInputEditText);


        oldPasswordWrapper = findViewById(R.id.fOldPassword).findViewById(R.id.textInputLayout);
        fOldPassword = findViewById(R.id.fOldPassword).findViewById(R.id.textInputEditText);


        btnSave = findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.savePressed(imageSelected);
            }
        });

        civProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.selectProfilePicturePressed();
            }
        });

        fBirthdate.setFocusable(false);

        birthdateWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        fBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });


        setHints();
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showUserData(User userData) {
        fFirstName.setText(userData.getFirstName());
        fLastName.setText(userData.getLastName());
        fBirthdate.setText(Formatter.formatBirthDayDate(Formatter.formatDate(userData.getBirthDate())));
        fCountry.setText(userData.getCountry());
        fEmail.setText(userData.getEmail());
        fEmail.setEnabled(false);

        if (!imageSelected){
            GlideImageView.loadImageById(this, userData.getProfilePictureJson(), civProfileImage);
            GlideImageView.loadBlurImageById(this, userData.getProfilePictureJson(), ivHeaderBackground, 50);
        }
    }

    private void setHints() {
        firstNameWrapper.setHint(getString(R.string.register_first_name));
        lastNameWrapper.setHint(getString(R.string.register_last_name));
        birthdateWrapper.setHint(getString(R.string.register_birthday));
        countryWrapper.setHint(getString(R.string.register_country));
        oldPasswordWrapper.setHint(getString(R.string.edit_profile_current_password));
        passwordWrapper.setHint(getString(R.string.edit_profile_new_password));
        emailWrapper.setHint(getString(R.string.register_email));
    }

    @Override
    public ModifyUserDto getAllData() {
        ModifyUserDto user = new ModifyUserDto();

        user.setFirstName(fFirstName.getText().toString());
        user.setLastName(fLastName.getText().toString());
        user.setBirthDate(fBirthdate.getText().toString());
        user.setCountry(fCountry.getText().toString());
        user.setEmail(fEmail.getText().toString());
        user.setOldPassword(fOldPassword.getText().toString());
        user.setNewPassword(fPassword.getText().toString());

        return user;
    }

    @Override
    public void showErrors(ArrayList<ProfileFields> fields) {
        for (int i = 0; i < fields.size(); i++) {
            switch (fields.get(i)) {
                case FIRST_NAME:
                    firstNameWrapper.setError(getString(R.string.edit_profile_firstname_error));
                    break;
                case LAST_NAME:
                    lastNameWrapper.setError(getString(R.string.edit_profile_lastname_error));
                    break;
                case EMAIL:
                    emailWrapper.setError(getString(R.string.edit_profile_email_error));
                    break;
                case COUNTRY:
                    countryWrapper.setError(getString(R.string.edit_profile_country_error));
                    break;
                case BIRTHDAY:
                    birthdateWrapper.setError(getString(R.string.edit_profile_birthday_error));
                    break;
                case PASSWORD:
                    oldPasswordWrapper.setError(getString(R.string.edit_profile_password_error));
                    break;
                case NEW_PASSWORD:
                    passwordWrapper.setError(getString(R.string.edit_profile_new_password_error));
                    break;
            }
        }
    }

    @Override
    public void hideAllErrors() {
        firstNameWrapper.setError(null);
        lastNameWrapper.setError(null);
        emailWrapper.setError(null);
        countryWrapper.setError(null);
        birthdateWrapper.setError(null);
        passwordWrapper.setError(null);
    }

    @Override
    public void showImagePicker() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        assert selectedImage != null;
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    selectedPicture = getResizedBitmap(BitmapFactory.decodeStream(imageStream), 400);

                    GlideImageView.loadLocalImage(this, selectedPicture, civProfileImage);
                    GlideImageView.loadLocalBlurImage(this, selectedPicture, ivHeaderBackground, 30);
                    imageSelected = true;
                }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void showDatePickerDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar birthdayCal = Calendar.getInstance();
                birthdayCal.set(Calendar.YEAR,year);
                birthdayCal.set(Calendar.MONTH,monthOfYear);
                birthdayCal.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                fBirthdate.setText(Formatter.formatDateString(new Date(birthdayCal.getTimeInMillis())));
            }
        };

        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpDialog.show();

    }

    @Override
    public Bitmap getTakenPicture() {
        return selectedPicture;
    }

}
