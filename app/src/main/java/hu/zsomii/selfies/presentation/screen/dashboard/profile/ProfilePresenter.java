package hu.zsomii.selfies.presentation.screen.dashboard.profile;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.profile.IProfileInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventProfileStatObtained;
import hu.zsomii.selfies.presentation.event.EventUserDataObtained;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class ProfilePresenter extends BasePresenter implements IProfilePresenter {

    @Inject
    IProfileInteractor interactor;

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    ISharedPref sharedPref;

    User previousProfileData;
    User newProfileData;

    public ProfilePresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        if (screen != null) {
            ((IProfileFragment) screen).showUserData(sessionInteractor.getCurrentUser());
            interactor.getProfileStat(null);
        }
    }

    @Override
    public void detachScreen() {
        super.detachScreen();
    }


    public void onEvent(EventProfileStatObtained event) {
        if (screen != null) {
            screen.hideLoader();
            ((IProfileFragment) screen).showProfileStat(event.getProfileStat());
        }
    }

    @Override
    public void editButtonPressed() {
        ((IProfileFragment) screen).goToEditProfile();
    }

    @Override
    public void showAlbumClicked() {
        sessionInteractor.setSelectedUserId(null);
        ((IProfileFragment) screen).goToAlbum();
    }
}
