package hu.zsomii.selfies.presentation.screen.fbregister;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

public interface IFbRegisterPresenter extends IBasePresenter {
    void checkFbToken(String token);
}
