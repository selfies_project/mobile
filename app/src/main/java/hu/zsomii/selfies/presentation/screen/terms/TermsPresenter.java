package hu.zsomii.selfies.presentation.screen.terms;

import hu.zsomii.selfies.presentation.base.BasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class TermsPresenter extends BasePresenter implements ITermsPresenter {

    @Override
    public void closePressed() {
        screen.finish();
    }
}
