package hu.zsomii.selfies.presentation.screen.dashboard.share;

import android.content.Context;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.interactor.share.IShareInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.event.EventLocationsDownloaded;
import hu.zsomii.selfies.presentation.event.EventSelfieUploadFailed;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class SharePresenter extends BasePresenter implements ISharePresenter {

    @Inject
    IShareInteractor interactor;
    @Inject
    ISessionInteractor sessionInteractor;
    @Inject
    Context context;

    public SharePresenter() {
        Selfies.injector().inject(this);
    }


    public void onEvent(EventLocationsDownloaded event) {
        if (screen != null) {
            screen.hideLoader();
            ((IShareFragment)screen).showAvailableLocations(event.getAvailableLocations());
        }
    }


    public void onEvent(EventSelfieUploadFailed event) {
        if (screen != null) {
            screen.hideLoader();
            screen.showToast(context.getString(R.string.upload_failed));
        }

    }

    @Override
    public void refreshAvailableLocation() {
        if (screen  != null){
            screen.showLoader();
        }
        interactor.getAvailableLocations();
    }

    @Override
    public void onShowLocationClicked(float lat, float lng, String name) {
        ((IShareFragment)screen).showMap(lat, lng, name);
    }


    @Override
    public void onTakePictureClicked(String locationId) {
        if (interactor.isCameraPermissionGranted()){
            sessionInteractor.saveSelfieLocation(locationId);
            ((IShareFragment)screen).startPictureTakingActivity();
        }
        else {
            screen.showToast(context.getString(R.string.take_photo_camera_not_enabled));
        }

    }
}
