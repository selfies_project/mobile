package hu.zsomii.selfies.presentation.screen.register;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface IRegisterPresenter  extends IBasePresenter{
    void termsClicked();
    void registerClicked();
    void chooseImageClicked();
}
