package hu.zsomii.selfies.presentation.screen.otherprofile;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.profile.IProfileInteractor;
import hu.zsomii.selfies.interactor.profile.event.EventOtherProfileObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherProfileStatObtained;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;

public class OtherProfilePresenter extends BasePresenter implements IOtherProfilePresenter {

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    IProfileInteractor profileInteractor;


    public OtherProfilePresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        if (screen != null) {
            screen.showLoader();
            profileInteractor.getOtherProfile(sessionInteractor.getSelectedUserId());
            profileInteractor.getProfileStat(sessionInteractor.getSelectedUserId());
        }
    }

    @Override
    public void detachScreen() {
        super.detachScreen();
    }


    public void onEvent(EventOtherProfileStatObtained event) {
        if (screen != null) {
            screen.hideLoader();
            ((IOtherProfileActivity) screen).showProfileStat(event.getProfileStat());
        }
    }

    public void onEvent(EventOtherProfileObtained event) {
        if (screen != null) {
            screen.hideLoader();
            ((IOtherProfileActivity) screen).showUserData(event.getOtherUser());
        }
    }

    @Override
    public void showAlbumClicked() {
        ((IOtherProfileActivity) screen).goToAlbum();
    }
}
