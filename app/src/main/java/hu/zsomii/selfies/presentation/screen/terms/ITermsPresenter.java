package hu.zsomii.selfies.presentation.screen.terms;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface ITermsPresenter extends IBasePresenter {
    void closePressed();
}
