package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.ToplistUserDto;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class EventToplistDownloaded {
    private ArrayList<ToplistUserDto> users;

    public ArrayList<ToplistUserDto> getUsers() {
        return users;
    }

    public EventToplistDownloaded(ArrayList<ToplistUserDto> users) {
        this.users = users;
    }
}
