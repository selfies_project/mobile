package hu.zsomii.selfies.presentation.event;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public class EventLoginFailed {
    String message;

    public EventLoginFailed(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
