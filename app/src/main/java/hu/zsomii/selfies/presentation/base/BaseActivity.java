package hu.zsomii.selfies.presentation.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import hu.zsomii.selfies.R;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseScreen {

    ProgressDialog dialog;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideHeader();
    }

    private void hideHeader() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void navigateTo(Class<?> target) {
        startActivity(new Intent(this, target));
    }

    @Override
    public void navigateWithFinish(Class<?> target) {
        startActivity(new Intent(this, target));
        finish();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().attachScreen(this);
    }

    @Override
    protected void onPause() {
        getPresenter().detachScreen();
        super.onPause();
    }

    public void showLoader() {
        Log.e(">> showLoader", getClass().getCanonicalName());
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(this.getString(R.string.loading_text) );
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void hideLoader() {
        Log.e(">> hideLoader", getClass().getCanonicalName());
        if (dialog != null) {
            dialog.hide();
        }
    }

    protected abstract IBasePresenter getPresenter();
}
