package hu.zsomii.selfies.presentation.base;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.EventBusException;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class BasePresenter implements IBasePresenter {

    protected IBaseScreen screen;

    @Inject
    EventBus eventBus;

    @Inject
    Context context;

    public BasePresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        Log.e(">> attachScreen", getClass().getCanonicalName());
        this.screen = screen;
        try {
            if (!eventBus.isRegistered(this)) eventBus.register(this);
        } catch (EventBusException ignored) {
        }
    }

    @Override
    public void detachScreen() {
        screen = null;
        Log.e(">> detachScreen", getClass().getCanonicalName());
        if (eventBus.isRegistered(this)) eventBus.unregister(this);
    }

    public void onEvent(EventBaseError baseError){
        screen.hideLoader();
        screen.showToast(context.getString(R.string.base_eror));
    }

}
