package hu.zsomii.selfies.presentation.screen.login;

import hu.zsomii.selfies.presentation.base.IBaseScreen;

/**
 * Created by Adam Varga on 2/13/2018.
 */

interface ILoginActivity extends IBaseScreen{
    String getEmail();
    String getPassword();
    void showEmailError(String errorText);
    void showPasswordError(String errorText);
    void hideEmailError();
    void hidePasswordError();
    boolean shouldStoreCredentials();
   void showPreviousCredentials(String email, String password);
}
