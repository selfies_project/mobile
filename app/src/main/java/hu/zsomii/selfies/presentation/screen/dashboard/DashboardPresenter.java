package hu.zsomii.selfies.presentation.screen.dashboard;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.location.ILocationInteractor;
import hu.zsomii.selfies.interactor.location.event.EventGeofenceLocationsDownloaded;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.repository.preferences.ISharedPref;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class DashboardPresenter extends BasePresenter implements IDashboardPresenter {

    @Inject
    ISharedPref sharedPref;

    @Inject
    ILocationInteractor locationInteractor;

    @Inject
    ISessionInteractor sessionInteractor;


    public DashboardPresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        if (!sessionInteractor.isGeofencesInitialized()) {
            if (screen != null) {
                screen.showLoader();
                locationInteractor.refreshLocations();
            }
            sessionInteractor.setGeofencesInitialized(true);
        }
    }

    public void onEvent(EventGeofenceLocationsDownloaded event) {
        screen.hideLoader();
        ((IDashboardActivity) screen).addGeofences(event.getLocations());
    }

    @Override
    public void permissionGranted(boolean b) {
        sharedPref.storeBoolean(Globals.CAMERA_PERMISSION_GRANTED, b);
    }
}
