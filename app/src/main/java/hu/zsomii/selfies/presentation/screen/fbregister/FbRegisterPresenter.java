package hu.zsomii.selfies.presentation.screen.fbregister;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.login.ILoginInteractor;
import hu.zsomii.selfies.interactor.register.IRegisterInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.appinit.AppInitActivity;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.event.EventLoginSuccess;
import hu.zsomii.selfies.presentation.screen.register.event.EventFbTokenExists;
import hu.zsomii.selfies.presentation.screen.register.event.EventFbTokenNotExists;

public class FbRegisterPresenter extends BasePresenter implements IFbRegisterPresenter{

    @Inject
    IRegisterInteractor registerInteractor;

    @Inject
    ILoginInteractor loginInteractor;

    @Inject
    ISessionInteractor sessionInteractor;

    public FbRegisterPresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void checkFbToken(String token) {
        registerInteractor.checkFbToken(token);
    }

    public void onEvent (EventFbTokenExists event){
        if (screen != null){
            screen.hideLoader();
           screen.finish();
            sessionInteractor.setJWTToken(event.getUser());
            loginInteractor.loginWithToken();
        }
    }

    public void onEvent (EventFbTokenNotExists event){
        if (screen != null){
            screen.hideLoader();
            ((IFbRegisterActivity)screen).navigateToRegister();
        }
    }

}
