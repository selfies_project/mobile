package hu.zsomii.selfies.presentation.screen.login;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.login.ILoginInteractor;
import hu.zsomii.selfies.presentation.appinit.AppInitActivity;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventLoginDetailsValidationFailed;
import hu.zsomii.selfies.presentation.event.EventLoginFailed;
import hu.zsomii.selfies.presentation.event.EventLoginSuccess;
import hu.zsomii.selfies.presentation.screen.terms.TermsActivity;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class LoginPresenter extends BasePresenter implements ILoginPresenter {

    @Inject
    ILoginInteractor interactor;

    @Inject
    Context context;

    private String email;
    private String password;

    public LoginPresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        email = interactor.getPreviouslyUsedEmail();
        password = interactor.getPreviouslyUsedPassword();

        if ((!email.isEmpty() && !password.isEmpty())) {
            ((ILoginActivity) screen).showPreviousCredentials(email, password);
        }
    }

    @Override
    public void loginPressed() {
        email = ((ILoginActivity) screen).getEmail();
        password = ((ILoginActivity) screen).getPassword();
        if (screen != null) {
            screen.showLoader();
        }
        interactor.loginUser(email, password,((ILoginActivity) screen).shouldStoreCredentials());
    }

    @Override
    public void termsPressed() {
        screen.navigateTo(TermsActivity.class);
    }

    public void onEvent(EventLoginSuccess event) {
        Log.e("EventLoginSuccess",""+screen);
        if (screen != null) {
            screen.hideLoader();
        }
        screen.navigateWithFinish(AppInitActivity.class);
    }

    public void onEvent(EventLoginDetailsValidationFailed event) {
        if (screen != null) {
            screen.hideLoader();

            if (event.getFields().contains(LoginFields.EMAIL))
                ((ILoginActivity) screen).showEmailError(context.getString(R.string.login_invalid_email));
            else ((ILoginActivity) screen).hideEmailError();

            if (event.getFields().contains(LoginFields.PASSWORD))
                ((ILoginActivity) screen).showPasswordError(context.getString(R.string.login_invalid_password));
            else ((ILoginActivity) screen).hidePasswordError();
        }
    }

    public void onEvent(EventLoginFailed event) {
        if (screen != null) {
            screen.hideLoader();
            screen.showToast(event.getMessage());
        }
    }

}





