package hu.zsomii.selfies.presentation.screen.dashboard.discover;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.Selfie;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public interface IDiscoverListFragment {
    void showDiscoverList(ArrayList<Selfie> list);
}
