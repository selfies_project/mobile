package hu.zsomii.selfies.presentation.screen.otherprofilealbum;

import java.util.ArrayList;

import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.stub.ImageDetailDto;

public interface IOtherProfileAlbumActivity  extends IBaseScreen {
    void showAlbum(ArrayList<String> images);
    void showImage(ImageDetailDto imageDetail, boolean canReport);
    void showReportPopup();
}
