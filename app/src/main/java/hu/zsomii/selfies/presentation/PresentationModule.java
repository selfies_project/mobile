package hu.zsomii.selfies.presentation;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.zsomii.selfies.presentation.appinit.AppInitPresenter;
import hu.zsomii.selfies.presentation.appinit.IAppInitPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.DashboardPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.IDashboardPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.DiscoverListPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.IDiscoverListPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.IProfilePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.ProfilePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.ISharePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.SharePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.ITakePicturePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.TakePicturePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.IToplistPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.ToplistPresenter;
import hu.zsomii.selfies.presentation.screen.editprofile.EditProfilePresenter;
import hu.zsomii.selfies.presentation.screen.editprofile.IEditProfilePresenter;
import hu.zsomii.selfies.presentation.screen.fbregister.FbRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.fbregister.IFbRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.login.ILoginPresenter;
import hu.zsomii.selfies.presentation.screen.login.LoginPresenter;
import hu.zsomii.selfies.presentation.screen.otherprofile.IOtherProfilePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofile.OtherProfilePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.IOtherProfileAlbumPresenter;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.OtherProfileAlbumPresenter;
import hu.zsomii.selfies.presentation.screen.register.IRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.RegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.succes.ISuccesRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.succes.SuccesRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.splash.ISplashScreenPresenter;
import hu.zsomii.selfies.presentation.screen.splash.SplashScreenPresenter;
import hu.zsomii.selfies.presentation.screen.terms.ITermsPresenter;
import hu.zsomii.selfies.presentation.screen.terms.TermsPresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

@Module
public class PresentationModule {

    public PresentationModule() {

    }

    @Provides
    @Singleton
    ISplashScreenPresenter providesISplashScreenPresenter() {
        return new SplashScreenPresenter();
    }

    @Provides
    @Singleton
    ITermsPresenter providesITermsPresenter() {
        return new TermsPresenter();
    }

    @Provides
    @Singleton
    ILoginPresenter providesILoginPresenter() {
        return new LoginPresenter();
    }

    @Provides
    @Singleton
    IRegisterPresenter providesIRegisterPresenter() {
        return new RegisterPresenter();
    }

    @Provides
    @Singleton
    IDashboardPresenter providesIDashboardPresenter() {
        return new DashboardPresenter();
    }

    @Provides
    @Singleton
    ISuccesRegisterPresenter providesISuccesRegisterPresenter() {
        return new SuccesRegisterPresenter();
    }

    @Provides
    @Singleton
    IToplistPresenter providesIToplistPresenter() {
        return new ToplistPresenter();
    }

    @Provides
    @Singleton
    IDiscoverListPresenter providesIDiscoverListPresenter() {
        return new DiscoverListPresenter();
    }

    @Provides
    @Singleton
    IProfilePresenter providesIProfilePresenter() {
        return new ProfilePresenter();
    }

    @Provides
    @Singleton
    ISharePresenter providesISharePresenter() {
        return new SharePresenter();
    }

    @Provides
    @Singleton
    ITakePicturePresenter providesITakePicturePresenter() {
        return new TakePicturePresenter();
    }

    @Provides
    @Singleton
    IEditProfilePresenter providesIEditProfilePresenter() {
        return new EditProfilePresenter();
    }

    @Provides
    @Singleton
    IAppInitPresenter providesIAppInitPresenter() {
        return new AppInitPresenter();
    }

    @Provides
    @Singleton
    IOtherProfilePresenter providesIOtherProfilePresenter() {
        return new OtherProfilePresenter();
    }

    @Provides
    @Singleton
    IOtherProfileAlbumPresenter providesIOtherProfileAlbumPresenter() {
        return new OtherProfileAlbumPresenter();
    }

    @Provides
    @Singleton
    IFbRegisterPresenter providesIFbRegisterPresenter() {
        return new FbRegisterPresenter();
    }
}
