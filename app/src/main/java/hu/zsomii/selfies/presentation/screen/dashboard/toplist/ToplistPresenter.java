package hu.zsomii.selfies.presentation.screen.dashboard.toplist;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.interactor.toplist.IToplistInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.event.EventToplistDownloaded;
import hu.zsomii.selfies.presentation.screen.otherprofile.OtherProfileActivity;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class ToplistPresenter extends BasePresenter implements IToplistPresenter {

    @Inject
    IToplistInteractor interactor;

    @Inject
    ISessionInteractor sessionInteractor;

    public ToplistPresenter() {
        Selfies.injector().inject(this);
    }


    @Override
    public void refreshTopList() {
        if (screen != null) {
            screen.showLoader();
        }
        interactor.getToplist();
    }

    @Override
    public void userClicked(String id) {
        if (!sessionInteractor.getCurrentUser().getId().equals(id)){
            sessionInteractor.setSelectedUserId(id);
            ((IToplistFragment)screen).goToOtherProfile();
        }
    }

    @Override
    public String getCurrentUserId() {
        return sessionInteractor.getCurrentUser().getId();
    }

    public void onEvent(EventToplistDownloaded event) {
        if (screen != null) {
            screen.hideLoader();
            ((IToplistFragment)screen).populateToplist(event.getUsers());
        }
    }
}
