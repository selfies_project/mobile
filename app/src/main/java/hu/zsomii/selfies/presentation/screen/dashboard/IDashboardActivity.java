package hu.zsomii.selfies.presentation.screen.dashboard;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.AvailableLocation;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public interface IDashboardActivity {
    void addGeofences(ArrayList<AvailableLocation> locations);
}
