package hu.zsomii.selfies.presentation.screen.splash;

import com.facebook.login.LoginResult;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.appinit.IAppInitInteractor;
import hu.zsomii.selfies.interactor.appinit.event.EventAppVersionDownloaded;
import hu.zsomii.selfies.interactor.login.event.EventLoginTokenFailed;
import hu.zsomii.selfies.interactor.login.event.EventLoginTokenSuccess;
import hu.zsomii.selfies.presentation.appinit.AppInitActivity;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventLoginSuccess;
import hu.zsomii.selfies.presentation.screen.fbregister.FbRegisterActivity;
import hu.zsomii.selfies.presentation.screen.login.LoginActivity;
import hu.zsomii.selfies.presentation.screen.register.RegisterActivity;
import hu.zsomii.selfies.presentation.screen.terms.TermsActivity;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class SplashScreenPresenter extends BasePresenter implements ISplashScreenPresenter {

    @Inject
    IAppInitInteractor appInitInteractor;


    public SplashScreenPresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        appInitInteractor.getVersion();
    }

    @Override
    public void loginPressed() {
        screen.navigateTo(LoginActivity.class);
    }

    @Override
    public void registerPressed() {
        if (screen != null){
            screen.navigateTo(RegisterActivity.class);
        }
    }

    @Override
    public void facebookLoginPressed() {

    }

    @Override
    public void termsPressed() {
        if (screen != null){
            screen.navigateTo(TermsActivity.class);
        }

    }

    @Override
    public void resultArrived(LoginResult loginResult) {

    }

    @Override
    public void acceptOldVersionClicked() {

        screen.finish();
        System.exit(0);
    }

    @Override
    public void fbRegisterPressed() {
        screen.navigateTo(FbRegisterActivity.class);
    }

    public void onEvent(EventAppVersionDownloaded event) {
        if (screen != null) {
            screen.hideLoader();
        }

        if (!event.getIsAppVersionOk()) {
            ((ISplashScreenActivity) screen).showOldVersionPopup();
        } else {
            if (screen != null){
                screen.showLoader();
            }
            appInitInteractor.tryLoginWithToken();
        }
    }

    public void onEvent(EventLoginTokenFailed event) {
        screen.hideLoader();
        if (screen != null) {
            screen.hideLoader();
        }
    }

    public void onEvent(EventLoginTokenSuccess event) {
        if (screen != null) {
            screen.hideLoader();
            screen.navigateWithFinish(AppInitActivity.class);
        }
    }


    public void onEvent(EventLoginSuccess event) {
        if (screen != null) {
            (screen).hideLoader();
            screen.navigateWithFinish(AppInitActivity.class);
        }
    }

}
