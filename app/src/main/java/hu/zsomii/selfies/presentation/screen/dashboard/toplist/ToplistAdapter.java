package hu.zsomii.selfies.presentation.screen.dashboard.toplist;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import hu.zsomii.selfies.R;
import hu.zsomii.selfies.globals.Formatter;
import hu.zsomii.selfies.stub.ToplistUserDto;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.utils.GlideImageView;

public class ToplistAdapter extends RecyclerView.Adapter<ToplistAdapter.MyViewHolder> {

    private ArrayList<ToplistUserDto> toplistUsers;
    private TextView tvPosition;
    private TextView tvUserName;
    private TextView tvUserDetails;
    private TextView tvScore;
    private RelativeLayout rlHolder;
    private CircleImageView ivProfilePicture;
    private Context ctx;
    private IToplistPresenter presenter;
    private String currentUserId;

    class MyViewHolder extends RecyclerView.ViewHolder {

        MyViewHolder(View view) {
            super(view);

            tvPosition = view.findViewById(R.id.tvPosition);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvUserDetails = view.findViewById(R.id.tvUserDetails);
            tvScore = view.findViewById(R.id.tvScore);
            rlHolder = view.findViewById(R.id.rlHolder);
            ivProfilePicture = view.findViewById(R.id.ivProfilePicture);
        }
    }


    ToplistAdapter(ArrayList<ToplistUserDto> toplistUsers, Context ctx, IToplistPresenter presenter) {
        this.toplistUsers = toplistUsers;
        this.ctx = ctx;
        this.presenter = presenter;
        currentUserId = presenter.getCurrentUserId();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_toplist_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ToplistUserDto member = toplistUsers.get(position);

        //Highlight if it's current user
        if (member.getId().equals(currentUserId)){
            rlHolder.setBackgroundColor( ContextCompat.getColor(ctx, R.color.toplist_current_user_color));
        }
        else {
            rlHolder.setBackgroundColor( ContextCompat.getColor(ctx, R.color.toplist_other_user_color));
        }

        tvPosition.setText(member.getRank()+"");
        tvUserName.setText(member.getFirstName() + " " + member.getLastName());
        tvUserDetails.setText(Formatter.calculateAge(member.getBirthDate()) + ", " + member.getCountry());
        tvScore.setText(Long.toString(member.getScore()));

        rlHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.userClicked(member.getId());
            }
        });

        GlideImageView.loadImageById(ctx, member.getProfilePictureJson(), ivProfilePicture);
    }

    @Override
    public int getItemCount() {
        return toplistUsers.size();
    }


}