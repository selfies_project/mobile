package hu.zsomii.selfies.presentation.base;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface IBasePresenter {
    void attachScreen(IBaseScreen screen);
    void detachScreen();
}
