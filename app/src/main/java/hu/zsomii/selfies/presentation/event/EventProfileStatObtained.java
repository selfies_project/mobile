package hu.zsomii.selfies.presentation.event;

import hu.zsomii.selfies.stub.ProfileStat;

/**
 * Created by Adam Varga on 4/4/2018.
 */

public class EventProfileStatObtained {
    private ProfileStat profileStat;

    public EventProfileStatObtained(ProfileStat profileStat) {

        this.profileStat = profileStat;
    }

    public ProfileStat getProfileStat() {
        return profileStat;
    }

    public void setProfileStat(ProfileStat profileStat) {
        this.profileStat = profileStat;
    }
}
