package hu.zsomii.selfies.presentation.screen.dashboard.discover;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseFragment;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.stub.Selfie;

/**
 * Created by Adam Varga on 2/18/2018.
 */

public class DiscoverListFragment extends BaseFragment implements IDiscoverListFragment {

    @Inject
    IDiscoverListPresenter presenter;

    RecyclerView rvDiscover;

    public DiscoverListFragment() {
        Selfies.injector().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_discover_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvDiscover = view.findViewById(R.id.rvDiscover);
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showDiscoverList(ArrayList<Selfie> list) {
        DiscoverAdapter adapter = new DiscoverAdapter(list, getContext(),presenter);
        rvDiscover.setAdapter(adapter);
        rvDiscover.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
