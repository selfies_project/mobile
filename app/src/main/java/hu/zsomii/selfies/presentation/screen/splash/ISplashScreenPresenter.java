package hu.zsomii.selfies.presentation.screen.splash;

import com.facebook.login.LoginResult;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface ISplashScreenPresenter extends IBasePresenter{
    void loginPressed();
    void registerPressed();
    void facebookLoginPressed();
    void termsPressed();
    void resultArrived(LoginResult loginResult);
    void acceptOldVersionClicked();
    void fbRegisterPressed();
}
