package hu.zsomii.selfies.presentation.screen.dashboard.toplist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.base.BaseFragment;
import hu.zsomii.selfies.presentation.base.IBasePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofile.OtherProfileActivity;
import hu.zsomii.selfies.stub.ToplistUserDto;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 2/13/2018.
 */

public class ToplistFragment extends BaseFragment implements IToplistFragment {

    @Inject
    IToplistPresenter presenter;
    RecyclerView rvToplist;


    public ToplistFragment() {
        Selfies.injector().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_toplist, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvToplist = view.findViewById(R.id.rvToplist);

        presenter.refreshTopList();
    }

    @Override
    public void populateToplist(ArrayList<ToplistUserDto> toplistUsers) {
        ToplistAdapter adapter = new ToplistAdapter(toplistUsers, getContext(),presenter);
        rvToplist.setAdapter(adapter);
        rvToplist.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void goToOtherProfile() {
        startActivity(new Intent(getContext(), OtherProfileActivity.class));
    }

    @Override
    protected IBasePresenter getPresenter() {
        return presenter;
    }
}
