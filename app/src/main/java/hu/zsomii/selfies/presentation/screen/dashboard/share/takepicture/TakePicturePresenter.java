package hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture;

import android.graphics.Bitmap;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.share.IShareInteractor;
import hu.zsomii.selfies.interactor.share.takepicture.ITakePictureInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventSelfieUploadSucces;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class TakePicturePresenter extends BasePresenter implements ITakePicturePresenter {


    @Inject
    ITakePictureInteractor interactor;

    @Inject
    IShareInteractor shareInteractor;

    public TakePicturePresenter() {
        Selfies.injector().inject(this);
    }

    private Bitmap bitmap;

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
    }

    @Override
    public void detachScreen() {

    }

    @Override
    public void uploadClicked(Bitmap file) {
       if (screen != null){
           screen.showLoader();
       }
       interactor.uploadSelfie(file);
    }

    public void onEvent(EventSelfieUploadSucces event) {
        if (screen != null) {
            screen.hideLoader();
        }
        shareInteractor.refreshAvailableLocations();
        screen.finish();
    }
}
