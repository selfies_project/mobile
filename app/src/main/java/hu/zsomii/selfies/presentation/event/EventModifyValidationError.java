package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.interactor.register.ProfileFields;

/**
 * Created by Adam Varga on 4/8/2018.
 */

public class EventModifyValidationError {
    private ArrayList<ProfileFields> fields;

    public EventModifyValidationError(ArrayList<ProfileFields> fields) {
        this.fields = fields;
    }

    public ArrayList<ProfileFields> getFields() {
        return fields;
    }
}
