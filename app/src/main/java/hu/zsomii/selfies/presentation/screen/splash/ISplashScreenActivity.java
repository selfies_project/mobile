package hu.zsomii.selfies.presentation.screen.splash;

import hu.zsomii.selfies.presentation.base.IBaseScreen;

/**
 * Created by Adam Varga on 2/12/2018.
 */

public interface ISplashScreenActivity extends IBaseScreen {
    void showOldVersionPopup();
}
