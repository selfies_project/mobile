package hu.zsomii.selfies.presentation.screen.dashboard.discover;

import hu.zsomii.selfies.presentation.base.IBasePresenter;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public interface IDiscoverListPresenter extends IBasePresenter {
    void heartIconClicked(String imageId,boolean isAlreadyLiked);
}
