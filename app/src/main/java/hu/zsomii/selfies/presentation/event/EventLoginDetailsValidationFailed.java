package hu.zsomii.selfies.presentation.event;

import java.util.ArrayList;

import hu.zsomii.selfies.presentation.screen.login.LoginFields;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public class EventLoginDetailsValidationFailed {
    ArrayList<LoginFields> fields;

    public EventLoginDetailsValidationFailed(ArrayList<LoginFields> fields) {
        this.fields = fields;
    }

    public ArrayList<LoginFields> getFields() {
        return fields;
    }
}
