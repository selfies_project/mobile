package hu.zsomii.selfies.presentation.screen.dashboard.profile;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public enum ProfileState {
    CHECK, EDIT
}
