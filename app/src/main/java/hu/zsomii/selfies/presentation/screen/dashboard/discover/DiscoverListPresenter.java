package hu.zsomii.selfies.presentation.screen.dashboard.discover;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.discover.IDiscoverInteractor;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.base.IBaseScreen;
import hu.zsomii.selfies.presentation.event.EventDiscoverItemsDownloaded;
import hu.zsomii.selfies.presentation.event.EventLikeSendSucces;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class DiscoverListPresenter extends BasePresenter implements IDiscoverListPresenter {

    @Inject
    IDiscoverInteractor interactor;

    public DiscoverListPresenter() {
        Selfies.injector().inject(this);
    }

    @Override
    public void attachScreen(IBaseScreen screen) {
        super.attachScreen(screen);
        interactor.getDiscoverItems(0, 20);
    }

    public void onEvent(EventDiscoverItemsDownloaded event) {
        if (screen != null) {
            screen.hideLoader();
            ((IDiscoverListFragment) screen).showDiscoverList(event.getSelfies());
        }
    }

    public void onEvent(EventLikeSendSucces event) {
        if (screen != null) {
            screen.hideLoader();
            interactor.getDiscoverItems(0, 20);
        }
    }

    @Override
    public void heartIconClicked(String imageId, boolean isAlreadyLiked) {
        interactor.sendLikeClicked(imageId, isAlreadyLiked);
    }

}
