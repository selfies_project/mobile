package hu.zsomii.selfies.repository.preferences;

/**
 * Created by Adam Varga on 3/11/2018.
 */

public interface ISharedPref {
    //string
    void storeString(String key, String value);
    String getString(String key);
    void removeString(String key);

    //boolean
    void storeBoolean(String key, boolean b);
    boolean getBoolean(String key);
}
