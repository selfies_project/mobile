package hu.zsomii.selfies.repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.repository.preferences.SharedPref;

/**
 * Created by Adam Varga on 3/16/2018.
 */


@Module
public class RepositoryModule {

    public RepositoryModule() {

    }

    @Provides
    @Singleton
    ISharedPref providesISharedPreferences() {
        return new SharedPref();
    }
}
