package hu.zsomii.selfies.repository.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;

/**
 * Created by Adam Varga on 3/11/2018.
 */

public class SharedPref implements ISharedPref {

    @Inject
    Context context;

    public SharedPref() {
        Selfies.injector().inject(this);
    }

    @Override
    public void storeString(String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    @Override
    public String getString(String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    @Override
    public void removeString(String key) {
        storeString(key, "");
    }

    @Override
    public void storeBoolean(String key, boolean b) {
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, b);
        editor.apply();
    }

    @Override
    public boolean getBoolean(String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(key, false);
    }
}
