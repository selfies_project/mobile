package hu.zsomii.selfies;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import hu.zsomii.selfies.interactor.InteractorModule;
import hu.zsomii.selfies.presentation.PresentationModule;
import hu.zsomii.selfies.repository.RepositoryModule;
import hu.zsomii.selfies.utils.UtilsModule;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public class Selfies extends MultiDexApplication {

    private static IComponent iComponent;

    private static Context appContext;


    @Override
    public void onCreate() {
        super.onCreate();

        iComponent = DaggerIComponent.builder()
                .presentationModule(new PresentationModule())
                .interactorModule(new InteractorModule())
                .repositoryModule(new RepositoryModule())
                .utilsModule(new UtilsModule(getApplicationContext()))
                .build();

        appContext = getApplicationContext();

    }

    public static IComponent injector() {
        return iComponent;
    }

    public static void setInjector(IComponent component){
        iComponent = component;
    }

    public static Context getContext() {
        return appContext;
    }
}
