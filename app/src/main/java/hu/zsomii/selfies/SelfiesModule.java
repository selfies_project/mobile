package hu.zsomii.selfies;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Adam Varga on 3/3/2018.
 */
@Module
public class SelfiesModule {

    private Application mApplication;

    public SelfiesModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

}