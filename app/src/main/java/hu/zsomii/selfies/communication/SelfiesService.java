package hu.zsomii.selfies.communication;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.AvailableLocation;
import hu.zsomii.selfies.stub.ImageDetailDto;
import hu.zsomii.selfies.stub.LoggedInUserDto;
import hu.zsomii.selfies.stub.LoginUserDto;
import hu.zsomii.selfies.stub.ModifyUserDto;
import hu.zsomii.selfies.stub.OtherProfileDto;
import hu.zsomii.selfies.stub.ProfileStat;
import hu.zsomii.selfies.stub.ProfileStatDto;
import hu.zsomii.selfies.stub.RegisterUserDto;
import hu.zsomii.selfies.stub.ReportDto;
import hu.zsomii.selfies.stub.Selfie;
import hu.zsomii.selfies.stub.ToplistUserDto;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.stub.UserDto;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Adam Varga on 3/3/2018.
 */

public interface SelfiesService {

    //region AuthController
    @GET("auth/test")
    Call<String> test();

    @POST("auth/login")
    Call<LoggedInUserDto> login(@Body LoginUserDto loginUserDto);

    @GET("auth/login/token")
    Call<LoggedInUserDto> loginToken(@Header("Authorization")String jwtToken, @Query("pushToken")String pushToken);

    @POST("auth/logout")
    Call<ResponseBody> logout();

    @GET("auth/version")
    Call<String> getVersion();

    @Multipart
    @POST("auth/register")
    Call<ResponseBody> register(@Part MultipartBody.Part image, @Part("user") RegisterUserDto user);

    @Multipart
    @POST("auth/modify")
    Call<UserDto> modifyUser(@Part MultipartBody.Part image, @Part("user") ModifyUserDto user);

    @GET("auth/fbtoken")
    Call<String> checkFbToken(@Query("fbToken") String fbToken);
    //endregion

    //region SelfiesController
    @GET("selfies/locations")
    Call<ArrayList<AvailableLocation>> getAvailableLocations(@Header("Authorization")String jwtToken);

    @POST("selfies/like/send")
    Call<ResponseBody> sendLike(@Header("Authorization")String jwtToken, @Query("targetImageId") String targetImageId);

    @POST("selfies/like/delete")
    Call<ResponseBody> deleteLike(@Header("Authorization")String jwtToken, @Query("targetImageId") String targetImageId);

    @GET("selfies/discover")
    Call<ArrayList<Selfie>> discoverItems(@Header("Authorization")String jwtToken,@Query("skip") int skip, @Query("take") int take);

    @GET("selfies/toplist")
    Call<ArrayList<ToplistUserDto>> getToplist(@Header("Authorization")String jwtToken);

    @GET("selfies/profile/stat")
    Call<ProfileStatDto> getProfileStat(@Header("Authorization")String jwtToken,@Query("targetUserId") String targetUserId);

    @GET("selfies/profile/images")
    Call<ArrayList<String>> getImagesForUser(@Header("Authorization")String jwtToken,@Query("userId")String userId);

    @POST("selfies/report")
    Call<ResponseBody> reportUser(@Header("Authorization")String jwtToken, @Body ReportDto report);

    @GET("selfies/profile/image/detail")
    Call<ImageDetailDto> getImageDetail(@Header("Authorization")String jwtToken, @Query("imageId")String imageId);

    @GET("selfies/profile/other")
    Call<OtherProfileDto> getOthersProfile(@Header("Authorization")String jwtToken,@Query("userId") String userId);

    @Multipart
    @POST("selfies/upload")
    Call<ResponseBody> uploadSelfie(@Header("Authorization")String jwtToken,@Part MultipartBody.Part image, @Part("locationId") String locationId);
    //endregion
}