package hu.zsomii.selfies;

import javax.inject.Singleton;

import dagger.Component;
import de.greenrobot.event.EventBus;
import hu.zsomii.selfies.interactor.InteractorModule;
import hu.zsomii.selfies.interactor.appinit.AppInitInteractor;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.discover.DiscoverInteractor;
import hu.zsomii.selfies.interactor.editprofile.EditProfileInteractor;
import hu.zsomii.selfies.interactor.location.LocationInteractor;
import hu.zsomii.selfies.interactor.login.LoginInteractor;
import hu.zsomii.selfies.interactor.profile.ProfileInteractor;
import hu.zsomii.selfies.interactor.register.RegisterInteractor;
import hu.zsomii.selfies.interactor.session.SessionInteractor;
import hu.zsomii.selfies.interactor.share.ShareInteractor;
import hu.zsomii.selfies.interactor.share.takepicture.TakePictureInteractor;
import hu.zsomii.selfies.interactor.toplist.ToplistInteractor;
import hu.zsomii.selfies.presentation.PresentationModule;
import hu.zsomii.selfies.presentation.appinit.AppInitActivity;
import hu.zsomii.selfies.presentation.appinit.AppInitPresenter;
import hu.zsomii.selfies.presentation.base.BasePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.DashboardActivity;
import hu.zsomii.selfies.presentation.screen.dashboard.DashboardPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.DiscoverListFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.discover.DiscoverListPresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.ProfileFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.profile.ProfilePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.ShareFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.share.SharePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.TakePictureActivity;
import hu.zsomii.selfies.presentation.screen.dashboard.share.takepicture.TakePicturePresenter;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.ToplistFragment;
import hu.zsomii.selfies.presentation.screen.dashboard.toplist.ToplistPresenter;
import hu.zsomii.selfies.presentation.screen.editprofile.EditProfileActivity;
import hu.zsomii.selfies.presentation.screen.editprofile.EditProfilePresenter;
import hu.zsomii.selfies.presentation.screen.fbregister.FbRegisterActivity;
import hu.zsomii.selfies.presentation.screen.fbregister.FbRegisterPresenter;
import hu.zsomii.selfies.presentation.screen.login.LoginActivity;
import hu.zsomii.selfies.presentation.screen.login.LoginPresenter;
import hu.zsomii.selfies.presentation.screen.otherprofile.OtherProfileActivity;
import hu.zsomii.selfies.presentation.screen.otherprofile.OtherProfilePresenter;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.OtherProfileAlbumActivity;
import hu.zsomii.selfies.presentation.screen.otherprofilealbum.OtherProfileAlbumPresenter;
import hu.zsomii.selfies.presentation.screen.register.RegisterActivity;
import hu.zsomii.selfies.presentation.screen.register.RegisterPresenter;
import hu.zsomii.selfies.presentation.screen.register.succes.SuccesRegisterActivity;
import hu.zsomii.selfies.presentation.screen.splash.SplashScreenActivity;
import hu.zsomii.selfies.presentation.screen.splash.SplashScreenPresenter;
import hu.zsomii.selfies.presentation.screen.terms.TermsActivity;
import hu.zsomii.selfies.push.SelfiesFireBaseMessagingService;
import hu.zsomii.selfies.repository.RepositoryModule;
import hu.zsomii.selfies.repository.preferences.SharedPref;
import hu.zsomii.selfies.services.GeofenceTransitionsIntentService;
import hu.zsomii.selfies.utils.UtilsModule;

/**
 * Created by Adam Varga on 3/3/2018.
 */
@Singleton
@Component(modules = {PresentationModule.class, InteractorModule.class, RepositoryModule.class, UtilsModule.class})
public interface IComponent {

    //Activities
    void inject(SplashScreenActivity splashScreenActivity);
    void inject(TermsActivity termsActivity);
    void inject(LoginActivity loginActivity);
    void inject(RegisterActivity registerActivity);
    void inject(DashboardActivity dashboardActivity);
    void inject(SuccesRegisterActivity succesRegisterActivity);
    void inject(TakePictureActivity takePictureActivity);
    void inject(EditProfileActivity editProfileActivity);
    void inject(AppInitActivity appInitActivity);
    void inject(OtherProfileActivity otherProfileActivity);
    void inject(OtherProfileAlbumActivity otherProfileAlbumActivity);
    void inject (FbRegisterActivity fbRegisterActivity);

    //Fragments
    void inject(ToplistFragment toplistFragment);
    void inject(DiscoverListFragment discoverListFragment);
    void inject(ProfileFragment profileFragment);
    void inject(ShareFragment shareFragment);

    //Presenters
    void inject(BasePresenter basePresenter);
    void inject(LoginPresenter loginPresenter);
    void inject(RegisterPresenter registerPresenter);
    void inject(ToplistPresenter toplistPresenter);
    void inject(ProfilePresenter profilePresenter);
    void inject(SharePresenter sharePresenter);
    void inject(TakePicturePresenter takePicturePresenter);
    void inject(DiscoverListPresenter discoverListPresenter);
    void inject(EditProfilePresenter editProfilePresenter);
    void inject(AppInitPresenter appInitPresenter);
    void inject(DashboardPresenter dashboardPresenter);
    void inject(SplashScreenPresenter splashScreenPresenter);
    void inject(OtherProfilePresenter otherProfilePresenter);
    void inject(OtherProfileAlbumPresenter otherProfileAlbumPresenter);
    void inject (FbRegisterPresenter fbRegisterPresenter);

    //Interactors
    void inject(LoginInteractor loginInteractor);
    void inject(BaseInteractor baseInteractor);
    void inject(RegisterInteractor registerInteractor);
    void inject(ToplistInteractor toplistInteractor);
    void inject(ProfileInteractor profileInteractor);
    void inject(SessionInteractor sessionInteractor);
    void inject(ShareInteractor shareInteractor);
    void inject(TakePictureInteractor takePictureInteractor);
    void inject(DiscoverInteractor discoverInteractor);
    void inject(EditProfileInteractor editProfileInteractor);
    void inject(AppInitInteractor appInitInteractor);
    void inject(LocationInteractor locationInteractor);

    //Repository
    void inject(SharedPref sharedPreferences);
    void inject(EventBus eventbus);

    //Service
    void inject(SelfiesFireBaseMessagingService selfiesFireBaseMessagingService);
    void inject(GeofenceTransitionsIntentService geofenceTransitionsIntentService);

}
