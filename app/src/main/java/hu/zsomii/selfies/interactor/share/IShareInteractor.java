package hu.zsomii.selfies.interactor.share;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public interface IShareInteractor {
    void getAvailableLocations();
    boolean isCameraPermissionGranted();
    void refreshAvailableLocations();
}
