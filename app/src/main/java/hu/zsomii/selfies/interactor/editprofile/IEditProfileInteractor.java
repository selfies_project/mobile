package hu.zsomii.selfies.interactor.editprofile;

import android.graphics.Bitmap;

import hu.zsomii.selfies.stub.ModifyUserDto;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public interface IEditProfileInteractor {
    User currentUserData();
    void modifyUserData(Bitmap image, ModifyUserDto user);
}
