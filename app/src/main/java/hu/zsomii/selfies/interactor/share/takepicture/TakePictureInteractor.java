package hu.zsomii.selfies.interactor.share.takepicture;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventSelfieUploadFailed;
import hu.zsomii.selfies.presentation.event.EventSelfieUploadSucces;
import hu.zsomii.selfies.stub.User;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class TakePictureInteractor extends BaseInteractor implements ITakePictureInteractor {

    @Inject
    ISessionInteractor sessionInteractor;
    @Inject
    Context context;


    public TakePictureInteractor() {
        Selfies.injector().inject(this);
    }

    @Override
    public void uploadSelfie(Bitmap uri) {

        File file = getFile(context, uri);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), requestFile);
        String locationId = sessionInteractor.getSelfieLocation();

        getServices().uploadSelfie(sessionInteractor.getJWTToken(), body, locationId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                eventbus.post(new EventSelfieUploadSucces());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                eventbus.post(new EventSelfieUploadFailed());
            }


        });
    }

    private File getFile(Context context, Bitmap bitmap) {
        File f = new File(context.getCacheDir(), "selfie.png");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

}

