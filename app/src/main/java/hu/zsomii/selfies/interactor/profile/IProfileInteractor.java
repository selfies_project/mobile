package hu.zsomii.selfies.interactor.profile;

import hu.zsomii.selfies.stub.ReportDto; /**
 * Created by Adam Varga on 3/16/2018.
 */

public interface IProfileInteractor {
    void getProfileStat(final String targetUserId);
    void getOtherProfile(final String userId);
    void getImagesForUser();
    void getImageDetails();
    void sendReport(ReportDto reportDto);
}
