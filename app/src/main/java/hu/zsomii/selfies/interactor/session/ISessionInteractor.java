package hu.zsomii.selfies.interactor.session;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.AvailableLocation;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/16/2018.
 */

public interface ISessionInteractor {

    User getCurrentUser();

    void saveSelfieLocation(String locationId);

    String getSelfieLocation();

    void storeCurrentUser(User user);

    void storeAvailableLocations(ArrayList<AvailableLocation> availableLocations);

    ArrayList<AvailableLocation> getAvailableLocations();

    String getJWTToken();

    void setJWTToken(String JWTToken);

    void setSelectedUserId(String id);

    String getSelectedUserId();

    void setSelectedImage(String id);

    String getSelectedImageId();

    boolean isGeofencesInitialized();

    void setGeofencesInitialized(boolean geofencesInitialized);

}
