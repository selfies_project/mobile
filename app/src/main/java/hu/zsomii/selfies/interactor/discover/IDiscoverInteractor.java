package hu.zsomii.selfies.interactor.discover;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public interface IDiscoverInteractor {
    void getDiscoverItems(int skip, int take);
    void sendLikeClicked(String imageId,boolean isAlreadyLiked);
}
