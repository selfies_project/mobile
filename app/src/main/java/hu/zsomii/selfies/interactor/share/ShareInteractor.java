package hu.zsomii.selfies.interactor.share;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.LocationChangedCallback;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.location.ILocationInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventLocationsDownloaded;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.AvailableLocation;
import hu.zsomii.selfies.stub.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class ShareInteractor extends BaseInteractor implements IShareInteractor {

    @Inject
    Context context;

    @Inject
    ISharedPref sharedPref;

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    ILocationInteractor locationInteractor;

    private ArrayList<AvailableLocation> locations;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationChangedCallback callback;

    public ShareInteractor() {
        Selfies.injector().inject(this);
        callback = new LocationChangedCallback() {
            @Override
            public void onLocationChanged(Location location) {
                if (locations != null){

                    Location tempLoc;
                    for (AvailableLocation it : locations) {
                        tempLoc = new Location("");
                        tempLoc.setLatitude(it.getLatitude());
                        tempLoc.setLongitude(it.getLongitude());

                        it.setDistance( (location == null) ? 0 : tempLoc.distanceTo(location));
                    }
                }

                sortLocationsByDistance();
                eventbus.post(new EventLocationsDownloaded(locations));
            }
        };
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }

    private void sortLocationsByDistance() {
        Collections.sort(locations, new Comparator<AvailableLocation>(){
            public int compare(AvailableLocation al1, AvailableLocation al2){
                if(al1.getDistance() == al2.getDistance())
                    return 0;
                return al1.getDistance() < al2.getDistance() ? -1 : 1;
            }
        });
    }

    @Override
    public void getAvailableLocations() {
        getServices().getAvailableLocations(sessionInteractor.getJWTToken()).enqueue(new Callback<ArrayList<AvailableLocation>>() {
            @Override
            public void onResponse(Call<ArrayList<AvailableLocation>> call, Response<ArrayList<AvailableLocation>> response) {
                locations = response.body();
                getLastKnownLocation();
            }

            @Override
            public void onFailure(Call<ArrayList<AvailableLocation>> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }
        });
    }

    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        } else {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            callback.onLocationChanged(location);
                        }

                    });
        }
    }

    @Override
    public boolean isCameraPermissionGranted() {
        return sharedPref.getBoolean(Globals.CAMERA_PERMISSION_GRANTED);
    }

    @Override
    public void refreshAvailableLocations() {
        getAvailableLocations();
    }
}
