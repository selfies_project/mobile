package hu.zsomii.selfies.interactor.register;

/**
 * Created by Adam Varga on 3/9/2018.
 */

public enum ProfileFields {
    FIRST_NAME, LAST_NAME, EMAIL, COUNTRY, BIRTHDAY, PASSWORD, NEW_PASSWORD
}
