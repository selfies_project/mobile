package hu.zsomii.selfies.interactor.register;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.presentation.event.EventRegisterFailed;
import hu.zsomii.selfies.presentation.event.EventRegisterSucces;
import hu.zsomii.selfies.presentation.event.EventRegisterValidationError;
import hu.zsomii.selfies.presentation.screen.register.event.EventFbTokenExists;
import hu.zsomii.selfies.presentation.screen.register.event.EventFbTokenNotExists;
import hu.zsomii.selfies.stub.LoginUserDto;
import hu.zsomii.selfies.stub.RegisterUserDto;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.stub.UserDto;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 3/8/2018.
 */

public class RegisterInteractor extends BaseInteractor implements IRegisterInteractor {

    @Inject
    Context context;

    public RegisterInteractor() {
        Selfies.injector().inject(this);
    }

    @Override
    public void registerUser(User user, Bitmap profilePicture) {
        RegisterUserDto registerUserDto = modelMapper.map(user, RegisterUserDto.class);
        if (allDataValid(user)) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), getFile(context, profilePicture));
            MultipartBody.Part body = MultipartBody.Part.createFormData("upload", getFile(context, profilePicture).getName(), reqFile);

            Log.e("parsed", registerUserDto.toString());
            getServices().register(body, registerUserDto).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        eventbus.post(new EventRegisterSucces());
                    } else {
                        eventbus.post(new EventRegisterFailed("E-mail already taken!"));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    eventbus.post(new EventBaseError());
                }
            });
        }
    }

    @Override
    public void checkFbToken(String token) {
        getServices().checkFbToken(token).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body() == null) {
                    eventbus.post(new EventFbTokenNotExists());
                } else {
                    eventbus.post(new EventFbTokenExists(response.body()));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }
        });

    }

    private File getFile(Context context, Bitmap bitmap) {

        File f = new File(context.getCacheDir(), "avatar.png");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    boolean allDataValid(User dataToRegister) {
        ArrayList<ProfileFields> validationErrors = validateData(dataToRegister);

        if (!validationErrors.isEmpty()) {
            eventbus.post(new EventRegisterValidationError(validationErrors));
            return false;
        }
        return true;
    }


    public ArrayList<ProfileFields> validateData(User dataToRegister) {

        ArrayList<ProfileFields> validationErrors = new ArrayList<>();

        if (dataToRegister.getFirstName() == null || dataToRegister.getFirstName().isEmpty())
            validationErrors.add((ProfileFields.FIRST_NAME));

        if (dataToRegister.getLastName() == null || dataToRegister.getLastName().isEmpty())
            validationErrors.add((ProfileFields.LAST_NAME));

        if (dataToRegister.getCountry() == null || dataToRegister.getCountry().isEmpty())
            validationErrors.add((ProfileFields.COUNTRY));

        if (dataToRegister.getBirthDate() == null || dataToRegister.getBirthDate().isEmpty())
            validationErrors.add((ProfileFields.BIRTHDAY));

        if (dataToRegister.getEmail() == null || dataToRegister.getEmail().isEmpty()) {
            validationErrors.add((ProfileFields.EMAIL));
        } else {
            Pattern pattern = Pattern.compile(Globals.EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(dataToRegister.getEmail());
            if (!matcher.matches()) validationErrors.add((ProfileFields.EMAIL));
        }

        if (dataToRegister.getPassword() == null || dataToRegister.getPassword().length() < 5)
            validationErrors.add((ProfileFields.PASSWORD));


        return validationErrors;
    }
}
