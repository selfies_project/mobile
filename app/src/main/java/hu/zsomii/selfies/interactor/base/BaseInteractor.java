package hu.zsomii.selfies.interactor.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.modelmapper.ModelMapper;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.communication.SelfiesService;
import hu.zsomii.selfies.globals.Globals;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Adam Varga on 3/7/2018.
 */

public class BaseInteractor {

    @Inject
    public
    EventBus eventbus;

    @Inject
    public
    ModelMapper modelMapper;

    private SelfiesService service;
    private ObjectMapper objectMapper;

    public BaseInteractor() {

        Selfies.injector().inject(this);


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS).addInterceptor(interceptor).build();

        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        JacksonConverterFactory factory = JacksonConverterFactory.create(objectMapper);

        service = new Retrofit.Builder()
                .baseUrl(Globals.SERVICE_URL)
                .client(httpClient)
                .addConverterFactory(factory)
                .build()
                .create(SelfiesService.class);
    }


    public SelfiesService getServices() {
        return service;
    }

}
