package hu.zsomii.selfies.interactor.profile.event;

import hu.zsomii.selfies.stub.ImageDetailDto;

public class EventOtherUserImageDetailObtained {

    private ImageDetailDto imageDetail;

    public EventOtherUserImageDetailObtained(ImageDetailDto imageDetail) {
        this.imageDetail = imageDetail;
    }

    public ImageDetailDto getImageDetail() {
        return imageDetail;
    }

    public void setImageDetail(ImageDetailDto imageDetail) {
        this.imageDetail = imageDetail;
    }
}
