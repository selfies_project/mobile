package hu.zsomii.selfies.interactor.login;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.login.event.EventLoginTokenFailed;
import hu.zsomii.selfies.interactor.login.event.EventLoginTokenSuccess;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventLoginDetailsValidationFailed;
import hu.zsomii.selfies.presentation.event.EventLoginFailed;
import hu.zsomii.selfies.presentation.event.EventLoginSuccess;
import hu.zsomii.selfies.presentation.screen.login.LoginFields;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.LoggedInUserDto;
import hu.zsomii.selfies.stub.LoginUserDto;
import hu.zsomii.selfies.stub.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 3/7/2018.
 */

public class LoginInteractor extends BaseInteractor implements ILoginInteractor {

    @Inject
    Context context;

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    ISharedPref sharedPref;

    public LoginInteractor() {
        Selfies.injector().inject(this);
    }

    @Override
    public void loginUser(final String email, final String password, final boolean saveCredentials) {
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        ArrayList<LoginFields> invalidFields = invalidFields(email, password);

        if (invalidFields.isEmpty()) {

            LoginUserDto loginUserDto = new LoginUserDto();
            loginUserDto.setEmail(email);
            loginUserDto.setPassword(password);
            loginUserDto.setPushToken(pushToken);

            getServices().login(loginUserDto).enqueue(new Callback<LoggedInUserDto>() {
                @Override
                public void onResponse(Call<LoggedInUserDto> call, Response<LoggedInUserDto> response) {
                    Log.e("LoggedInUserDto", response.toString());
                    if (response.code() == 200) {
                        if (response.body() == null) {
                            eventbus.post(new EventLoginFailed(context.getString(R.string.login_invalid_credentials)));
                        } else {
                            sessionInteractor.setJWTToken(response.body().getToken());
                            sessionInteractor.storeCurrentUser(modelMapper.map(response.body().getUser(), User.class));
                            if (saveCredentials) {
                                sharedPref.storeString(Globals.LAST_LOGGED_IN_USER_EMAIL, email);
                                sharedPref.storeString(Globals.LAST_LOGGED_IN_USER_PASSWORD, password);
                            } else {
                                sharedPref.removeString(Globals.LAST_LOGGED_IN_USER_EMAIL);
                                sharedPref.removeString(Globals.LAST_LOGGED_IN_USER_PASSWORD);
                            }
                            eventbus.post(new EventLoginSuccess());
                            return;
                        }
                    } else{
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            eventbus.post(new EventLoginFailed(jObjError.getString("error")));
                            return;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        eventbus.post(new EventBaseError());
                        return;
                    }


                    eventbus.post(new EventLoginFailed(context.getString(R.string.login_invalid_credentials)));
                }

                @Override
                public void onFailure(Call<LoggedInUserDto> call, Throwable t) {
                    eventbus.post(new EventBaseError());
                }
            });
        } else {
            eventbus.post(new EventLoginDetailsValidationFailed((invalidFields)));
        }
    }

    public ArrayList<LoginFields> invalidFields(String email, String password) {
        ArrayList<LoginFields> invalidFields = new ArrayList<>();
        Pattern pattern = Pattern.compile(Globals.EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) invalidFields.add(LoginFields.EMAIL);
        if (password.length() < 5) invalidFields.add(LoginFields.PASSWORD);

        return invalidFields;
    }

    @Override
    public String getPreviouslyUsedEmail() {
        return sharedPref.getString(Globals.LAST_LOGGED_IN_USER_EMAIL);
    }

    @Override
    public String getPreviouslyUsedPassword() {
        return sharedPref.getString(Globals.LAST_LOGGED_IN_USER_PASSWORD);
    }

    @Override
    public void loginWithToken() {
        getServices().loginToken(sessionInteractor.getJWTToken(),FirebaseInstanceId.getInstance().getToken()).enqueue(new Callback<LoggedInUserDto>() {
            @Override
            public void onResponse(Call<LoggedInUserDto> call, Response<LoggedInUserDto> response) {
                if (response.code() == 200) {
                    sessionInteractor.setJWTToken(response.body().getToken());
                    sessionInteractor.storeCurrentUser(modelMapper.map(response.body().getUser(), User.class));
                    eventbus.post(new EventLoginTokenSuccess());
                } else {
                    eventbus.post(new EventLoginTokenFailed());
                }
            }

            @Override
            public void onFailure(Call<LoggedInUserDto> call, Throwable t) {
                eventbus.post(new EventLoginTokenFailed());
            }
        });
    }
}
