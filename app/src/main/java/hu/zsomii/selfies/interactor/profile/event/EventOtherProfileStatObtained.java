package hu.zsomii.selfies.interactor.profile.event;

import hu.zsomii.selfies.stub.ProfileStat;


public class EventOtherProfileStatObtained {
    private ProfileStat profileStat;

    public EventOtherProfileStatObtained(ProfileStat profileStat) {

        this.profileStat = profileStat;
    }

    public ProfileStat getProfileStat() {
        return profileStat;
    }

    public void setProfileStat(ProfileStat profileStat) {
        this.profileStat = profileStat;
    }
}