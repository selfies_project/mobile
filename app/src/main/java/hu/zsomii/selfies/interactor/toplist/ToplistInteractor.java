package hu.zsomii.selfies.interactor.toplist;

import org.modelmapper.TypeToken;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventToplistDownloaded;
import hu.zsomii.selfies.stub.ToplistUserDto;
import hu.zsomii.selfies.stub.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public class ToplistInteractor extends BaseInteractor implements IToplistInteractor {

    @Inject
    ISessionInteractor sessionInteractor;

    public ToplistInteractor() {
        Selfies.injector().inject(this);
    }

    @Override
    public void getToplist() {
        getServices().getToplist(sessionInteractor.getJWTToken()).enqueue(new Callback<ArrayList<ToplistUserDto>>() {
            @Override
            public void onResponse(Call<ArrayList<ToplistUserDto>> call, Response<ArrayList<ToplistUserDto>> response) {
                java.lang.reflect.Type targetListType = new TypeToken<ArrayList<ToplistUserDto>>() {}.getType();
                ArrayList<ToplistUserDto> users = modelMapper.map(response.body(), targetListType);
                eventbus.post(new EventToplistDownloaded(users));
            }

            @Override
            public void onFailure(Call<ArrayList<ToplistUserDto>> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }
        });
    }
}
