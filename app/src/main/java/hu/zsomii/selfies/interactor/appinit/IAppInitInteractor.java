package hu.zsomii.selfies.interactor.appinit;

/**
 * Created by Adam Varga on 4/8/2018.
 */

public interface IAppInitInteractor {
    void getAvailableLocations();
    void getVersion();
    void tryLoginWithToken();
}
