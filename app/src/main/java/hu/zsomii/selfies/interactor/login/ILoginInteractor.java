package hu.zsomii.selfies.interactor.login;

/**
 * Created by Adam Varga on 3/7/2018.
 */

public interface ILoginInteractor {
    void loginUser(String email, String password, boolean saveCredentials);
    String getPreviouslyUsedEmail();
    String getPreviouslyUsedPassword();
    void loginWithToken();
}
