package hu.zsomii.selfies.interactor.profile.event;

import java.util.ArrayList;

public class EventCurrentUserImagesObtained {

    private ArrayList<String> images;

    public EventCurrentUserImagesObtained(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
