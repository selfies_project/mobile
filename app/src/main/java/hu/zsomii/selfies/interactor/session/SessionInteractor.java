package hu.zsomii.selfies.interactor.session;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.presentation.event.EventInitialDataStored;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.AvailableLocation;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/16/2018.
 */

public class SessionInteractor extends BaseInteractor implements ISessionInteractor {

    @Inject
    ISharedPref preferences;

    private User currentUser;
    private String locationId;
    private ArrayList<AvailableLocation> availableLocations;
    private String selectedUserId;
    private String selectedImageId;
    private boolean geofencesInitialized;

    public boolean isGeofencesInitialized() {
        return geofencesInitialized;
    }

    public void setGeofencesInitialized(boolean geofencesInitialized) {
        this.geofencesInitialized = geofencesInitialized;
    }

    public SessionInteractor() {
        Selfies.injector().inject(this);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void saveSelfieLocation(String locationId) {
        this.locationId = locationId;
    }

    @Override
    public String getSelfieLocation() {
        return locationId;
    }

    @Override
    public void storeCurrentUser(User user) {
        this.currentUser = user;
    }

    @Override
    public void storeAvailableLocations(ArrayList<AvailableLocation> availableLocations) {
        this.availableLocations = availableLocations;
        eventbus.post(new EventInitialDataStored());
    }

    @Override
    public ArrayList<AvailableLocation> getAvailableLocations() {
        return availableLocations;
    }

    public String getJWTToken() {
        return preferences.getString(Globals.JWT_TOKEN_KEY);
    }

    public void setJWTToken(String JWTToken) {
        preferences.storeString(Globals.JWT_TOKEN_KEY, "Bearer " + JWTToken);
    }

    @Override
    public void setSelectedUserId(String id) {
        this.selectedUserId = id;
    }

    @Override
    public String getSelectedUserId() {
        return this.selectedUserId;
    }

    @Override
    public void setSelectedImage(String id) {
        this.selectedImageId = id;
    }

    @Override
    public String getSelectedImageId() {
        return this.selectedImageId;
    }

}
