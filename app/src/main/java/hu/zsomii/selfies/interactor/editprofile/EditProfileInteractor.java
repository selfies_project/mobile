package hu.zsomii.selfies.interactor.editprofile;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventModifyValidationError;
import hu.zsomii.selfies.presentation.event.EventProfileModificationFailed;
import hu.zsomii.selfies.presentation.event.EventProfileModificationSucces;
import hu.zsomii.selfies.stub.ModifyUserDto;
import hu.zsomii.selfies.stub.User;
import hu.zsomii.selfies.stub.UserDto;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public class EditProfileInteractor extends BaseInteractor implements IEditProfileInteractor {

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    Context context;


    public EditProfileInteractor() {
        Selfies.injector().inject(this);
    }


    @Override
    public User currentUserData() {
        return sessionInteractor.getCurrentUser();
    }


    @Override
    public void modifyUserData(Bitmap image, ModifyUserDto user) {

        if (allDataValid(user)) {
            if (image == null) {
                getServices().modifyUser(null, user).enqueue(new Callback<UserDto>() {

                    @Override
                    public void onResponse(Call<UserDto> call, Response<UserDto> response) {

                        if (response == null || response.body() == null) {
                            eventbus.post(new EventProfileModificationFailed());
                        } else if (response.body() != null) {
                            sessionInteractor.storeCurrentUser(modelMapper.map(response.body(), User.class));
                            eventbus.post(new EventProfileModificationSucces());
                        }

                    }

                    @Override
                    public void onFailure(Call<UserDto> call, Throwable t) {
                        eventbus.post(new EventBaseError());
                    }
                });
            } else {
                File f = getFile(context, image);
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);
                MultipartBody.Part body = MultipartBody.Part.createFormData("upload", f.getName(), reqFile);

                getServices().modifyUser(body, user).enqueue(new Callback<UserDto>() {

                    @Override
                    public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                        if (response.body() != null) {
                            sessionInteractor.storeCurrentUser(modelMapper.map(response.body(), User.class));
                        }
                        eventbus.post(new EventProfileModificationSucces());
                    }

                    @Override
                    public void onFailure(Call<UserDto> call, Throwable t) {
                        eventbus.post(new EventBaseError());
                    }
                });
            }
        }
    }

    private File getFile(Context context, Bitmap bitmap) {

        File f = new File(context.getCacheDir(), "avatar.png");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    private boolean allDataValid(ModifyUserDto user) {
        ArrayList<ProfileFields> validationErrors = validateData(user);

        if (!validationErrors.isEmpty()) {
            eventbus.post(new EventModifyValidationError(validationErrors));
            return false;
        }
        return true;
    }

    public ArrayList<ProfileFields> validateData(ModifyUserDto user) {

        ArrayList<ProfileFields> validationErrors = new ArrayList<>();

        if (user.getFirstName() == null || user.getFirstName().isEmpty())
            validationErrors.add((ProfileFields.FIRST_NAME));

        if (user.getLastName() == null || user.getLastName().isEmpty())
            validationErrors.add((ProfileFields.LAST_NAME));

        if (user.getCountry() == null || user.getCountry().isEmpty())
            validationErrors.add((ProfileFields.COUNTRY));

        if (user.getBirthDate() == null || user.getBirthDate().isEmpty())
            validationErrors.add((ProfileFields.BIRTHDAY));

        if (user.getEmail() == null || user.getEmail().isEmpty()) {
            validationErrors.add((ProfileFields.EMAIL));
        } else {
            Pattern pattern = Pattern.compile(Globals.EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(user.getEmail());
            if (!matcher.matches()) validationErrors.add((ProfileFields.EMAIL));
        }

        if (user.getOldPassword() != null && !user.getOldPassword().isEmpty() && !user.getOldPassword().equals("") && user.getOldPassword().length() < 5)
            validationErrors.add((ProfileFields.PASSWORD));

        if (user.getOldPassword() != null && !user.getOldPassword().isEmpty() && !user.getOldPassword().equals("") && user.getOldPassword().length() >= 5 && (user.getNewPassword() == null || !user.getNewPassword().equals("") && user.getNewPassword().length() < 5)) {
            validationErrors.add(ProfileFields.NEW_PASSWORD);
        }

        return validationErrors;
    }


}
