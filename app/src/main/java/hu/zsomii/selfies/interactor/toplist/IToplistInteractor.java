package hu.zsomii.selfies.interactor.toplist;

/**
 * Created by Adam Varga on 3/10/2018.
 */

public interface IToplistInteractor {
    void getToplist();
}
