package hu.zsomii.selfies.interactor.profile.event;

import hu.zsomii.selfies.stub.ImageDetailDto;

public class EventCurrentUserImageDetailObtained {


    private ImageDetailDto imageDetail;

    public EventCurrentUserImageDetailObtained(ImageDetailDto imageDetail) {
        this.imageDetail = imageDetail;
    }

    public ImageDetailDto getImageDetail() {
        return imageDetail;
    }

    public void setImageDetail(ImageDetailDto imageDetail) {
        this.imageDetail = imageDetail;
    }
}
