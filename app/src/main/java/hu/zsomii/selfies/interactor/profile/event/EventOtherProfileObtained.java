package hu.zsomii.selfies.interactor.profile.event;

import hu.zsomii.selfies.stub.User;

public class EventOtherProfileObtained {

    User otherUser;

    public EventOtherProfileObtained(User otherUser) {
        this.otherUser = otherUser;
    }

    public User getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(User otherUser) {
        this.otherUser = otherUser;
    }
}
