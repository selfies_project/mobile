package hu.zsomii.selfies.interactor.appinit;

import android.content.Context;
import android.content.pm.PackageManager;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.appinit.event.EventAppVersionDownloaded;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.login.ILoginInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventAppInitFailed;
import hu.zsomii.selfies.presentation.event.EventInitialDataStored;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.AvailableLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 4/8/2018.
 */

public class AppInitInteractor extends BaseInteractor implements IAppInitInteractor {

    @Inject
    ISessionInteractor sessionInteractor;

    @Inject
    ILoginInteractor loginInteractor;

    @Inject
    ISharedPref preferences;

    @Inject
    Context context;

    public AppInitInteractor() {
        Selfies.injector().inject(this);
    }

    @Override
    public void getAvailableLocations() {
        getServices().getAvailableLocations(sessionInteractor.getJWTToken()).enqueue(new Callback<ArrayList<AvailableLocation>>() {

            @Override
            public void onResponse(Call<ArrayList<AvailableLocation>> call, Response<ArrayList<AvailableLocation>> response) {
                sessionInteractor.storeAvailableLocations(response.body());
                eventbus.post(new EventInitialDataStored());            }

            @Override
            public void onFailure(Call<ArrayList<AvailableLocation>> call, Throwable t) {
                eventbus.post(new EventAppInitFailed());
            }
        });
    }

    @Override
    public void getVersion() {
        getServices().getVersion().enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.body() == null){
                    eventbus.post(new EventAppVersionDownloaded(true));
return;
                }
                eventbus.post(new EventAppVersionDownloaded(response.body().equals(getCurrentAppVersion())));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                eventbus.post(new EventAppInitFailed());
            }
        });
    }

    @Override
    public void tryLoginWithToken() {
        if (preferences.getString(Globals.JWT_TOKEN_KEY) != null){
            loginInteractor.loginWithToken();
        }
    }

    private String getCurrentAppVersion(){
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "UNKNOWN";
    }
}
