package hu.zsomii.selfies.interactor;

import android.location.Location;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public interface LocationChangedCallback {
    void onLocationChanged(Location location);
}
