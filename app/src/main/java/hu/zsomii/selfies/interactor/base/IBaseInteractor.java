package hu.zsomii.selfies.interactor.base;

import hu.zsomii.selfies.communication.SelfiesService;

/**
 * Created by Adam Varga on 3/8/2018.
 */

public interface IBaseInteractor {
    SelfiesService getServices();
}
