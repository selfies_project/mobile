package hu.zsomii.selfies.interactor.appinit.event;

public class EventAppVersionDownloaded {
    private boolean appVersionIsOK;

    public EventAppVersionDownloaded(boolean appVersionIsOK) {
        this.appVersionIsOK = appVersionIsOK;
    }

    public boolean getIsAppVersionOk() {
        return appVersionIsOK;
    }

    public void setIsAppVersionOk(boolean appVersion) {
        this.appVersionIsOK = appVersion;
    }
}
