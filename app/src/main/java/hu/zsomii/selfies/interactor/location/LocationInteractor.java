package hu.zsomii.selfies.interactor.location;

import android.content.Context;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.location.event.EventGeofenceLocationsDownloaded;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.stub.AvailableLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public class LocationInteractor extends BaseInteractor implements ILocationInteractor {

    @Inject
    Context context;

    @Inject
    ISessionInteractor sessionInteractor;

    public LocationInteractor() {
        Selfies.injector().inject(this);
    }


    @Override
    public void refreshLocations() {
        getServices().getAvailableLocations(sessionInteractor.getJWTToken()).enqueue(new Callback<ArrayList<AvailableLocation>>() {
            @Override
            public void onResponse(Call<ArrayList<AvailableLocation>> call, Response<ArrayList<AvailableLocation>> response) {
                eventbus.post(new EventGeofenceLocationsDownloaded(response.body()));
            }

            @Override
            public void onFailure(Call<ArrayList<AvailableLocation>> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }
        });
    }
}
