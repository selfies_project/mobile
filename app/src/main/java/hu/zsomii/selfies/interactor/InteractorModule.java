package hu.zsomii.selfies.interactor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.zsomii.selfies.interactor.appinit.AppInitInteractor;
import hu.zsomii.selfies.interactor.appinit.IAppInitInteractor;
import hu.zsomii.selfies.interactor.discover.DiscoverInteractor;
import hu.zsomii.selfies.interactor.discover.IDiscoverInteractor;
import hu.zsomii.selfies.interactor.editprofile.EditProfileInteractor;
import hu.zsomii.selfies.interactor.editprofile.IEditProfileInteractor;
import hu.zsomii.selfies.interactor.location.ILocationInteractor;
import hu.zsomii.selfies.interactor.location.LocationInteractor;
import hu.zsomii.selfies.interactor.login.ILoginInteractor;
import hu.zsomii.selfies.interactor.login.LoginInteractor;
import hu.zsomii.selfies.interactor.profile.IProfileInteractor;
import hu.zsomii.selfies.interactor.profile.ProfileInteractor;
import hu.zsomii.selfies.interactor.register.IRegisterInteractor;
import hu.zsomii.selfies.interactor.register.RegisterInteractor;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.interactor.session.SessionInteractor;
import hu.zsomii.selfies.interactor.share.IShareInteractor;
import hu.zsomii.selfies.interactor.share.ShareInteractor;
import hu.zsomii.selfies.interactor.share.takepicture.ITakePictureInteractor;
import hu.zsomii.selfies.interactor.share.takepicture.TakePictureInteractor;
import hu.zsomii.selfies.interactor.toplist.IToplistInteractor;
import hu.zsomii.selfies.interactor.toplist.ToplistInteractor;

/**
 * Created by Adam Varga on 3/7/2018.
 */
@Module
public class InteractorModule {

    public InteractorModule(){
    }

    @Provides
    @Singleton
    ILoginInteractor providesILoginInteractor() {
        return new LoginInteractor();
    }

    @Provides
    @Singleton
    public IRegisterInteractor providesIRegisterInteractor() {
        return new RegisterInteractor();
    }

    @Provides
    @Singleton
    IToplistInteractor providesIToplistInteractor() {
        return new ToplistInteractor();
    }

    @Provides
    @Singleton
    IProfileInteractor providesIProfileInteractor() {
        return new ProfileInteractor();
    }

    @Provides
    @Singleton
    ISessionInteractor providesISessionInteractor() {
        return new SessionInteractor();
    }

    @Provides
    @Singleton
    IShareInteractor providesIShareInteractor() {
        return new ShareInteractor();
    }

    @Provides
    @Singleton
    ITakePictureInteractor providesITakePictureInteractor() {
        return new TakePictureInteractor();
    }

    @Provides
    @Singleton
    IDiscoverInteractor providesIDiscoverInteractor() {
        return new DiscoverInteractor();
    }

    @Provides
    @Singleton
    IEditProfileInteractor providesIEditProfileInteractor() {
        return new EditProfileInteractor();
    }

    @Provides
    @Singleton
    ILocationInteractor providesILocationInteractor() {
        return new LocationInteractor();
    }

    @Provides
    @Singleton
    IAppInitInteractor providesIAppInitInteractor() {
        return new AppInitInteractor();
    }



}
