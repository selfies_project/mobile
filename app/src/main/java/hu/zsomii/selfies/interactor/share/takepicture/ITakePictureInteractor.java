package hu.zsomii.selfies.interactor.share.takepicture;

import android.graphics.Bitmap;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public interface ITakePictureInteractor {
    void uploadSelfie(Bitmap bitmap);
}
