package hu.zsomii.selfies.interactor.location;

/**
 * Created by Adam Varga on 4/5/2018.
 */

public interface ILocationInteractor {
    void refreshLocations();
}
