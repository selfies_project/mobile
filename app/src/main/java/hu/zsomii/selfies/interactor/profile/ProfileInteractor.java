package hu.zsomii.selfies.interactor.profile;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.profile.event.EventCurrentUserImageDetailObtained;
import hu.zsomii.selfies.interactor.profile.event.EventCurrentUserImagesObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherProfileObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherProfileStatObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherUserImageDetailObtained;
import hu.zsomii.selfies.interactor.profile.event.EventOtherUserImagesObtained;
import hu.zsomii.selfies.interactor.profile.event.EventReportSuccess;
import hu.zsomii.selfies.interactor.register.ProfileFields;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventModifyProfileValidationError;
import hu.zsomii.selfies.presentation.event.EventProfileStatObtained;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.ImageDetailDto;
import hu.zsomii.selfies.stub.OtherProfileDto;
import hu.zsomii.selfies.stub.ProfileStat;
import hu.zsomii.selfies.stub.ProfileStatDto;
import hu.zsomii.selfies.stub.ReportDto;
import hu.zsomii.selfies.stub.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Adam Varga on 3/16/2018.
 */

public class ProfileInteractor extends BaseInteractor implements IProfileInteractor {

    @Inject
    ISharedPref sharedPref;
    @Inject
    ISessionInteractor sessionInteractor;

    public ProfileInteractor() {
        Selfies.injector().inject(this);
    }

    @Override
    public void getProfileStat(final String targetUserId) {
        getServices().getProfileStat(sessionInteractor.getJWTToken(), targetUserId).enqueue(new Callback<ProfileStatDto>() {

            @Override
            public void onResponse(Call<ProfileStatDto> call, Response<ProfileStatDto> response) {
                if (targetUserId == null) {
                    eventbus.post(new EventProfileStatObtained(modelMapper.map(response.body(), ProfileStat.class)));
                } else {
                    eventbus.post(new EventOtherProfileStatObtained(modelMapper.map(response.body(), ProfileStat.class)));
                }
            }

            @Override
            public void onFailure(Call<ProfileStatDto> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }
        });
    }

    @Override
    public void getOtherProfile(String userId) {
        getServices().getOthersProfile(sessionInteractor.getJWTToken(), userId).enqueue(new Callback<OtherProfileDto>() {
            @Override
            public void onResponse(Call<OtherProfileDto> call, Response<OtherProfileDto> response) {
                eventbus.post(new EventOtherProfileObtained(modelMapper.map(response.body(), User.class)));
            }

            @Override
            public void onFailure(Call<OtherProfileDto> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }

        });
    }

    @Override
    public void getImagesForUser() {
        getServices().getImagesForUser(sessionInteractor.getJWTToken(), sessionInteractor.getSelectedUserId()).enqueue(new Callback<ArrayList<String>>() {
            @Override
            public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response) {

                if (sessionInteractor.getSelectedUserId() == null) {
                    eventbus.post(new EventCurrentUserImagesObtained(response.body()));
                } else {
                    eventbus.post(new EventOtherUserImagesObtained(response.body()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }

        });
    }

    @Override
    public void getImageDetails() {
        getServices().getImageDetail(sessionInteractor.getJWTToken(), sessionInteractor.getSelectedImageId()).enqueue(new Callback<ImageDetailDto>() {
            @Override
            public void onResponse(Call<ImageDetailDto> call, Response<ImageDetailDto> response) {

                if (sessionInteractor.getSelectedImageId() == null) {
                    eventbus.post(new EventCurrentUserImageDetailObtained(response.body()));
                } else {
                    eventbus.post(new EventOtherUserImageDetailObtained(response.body()));
                }
            }

            @Override
            public void onFailure(Call<ImageDetailDto> call, Throwable t) {

            }

        });
    }

    @Override
    public void sendReport(ReportDto reportDto) {
        reportDto.setRelatedPictureId(sessionInteractor.getSelectedImageId());
        getServices().reportUser(sessionInteractor.getJWTToken(), reportDto).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                eventbus.post(new EventReportSuccess());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }


    private boolean allDataValid(User userData) {
        ArrayList<ProfileFields> validationErrors = new ArrayList<>();

        if (userData.getFirstName().isEmpty())
            validationErrors.add((ProfileFields.FIRST_NAME));


        if (userData.getLastName().isEmpty())
            validationErrors.add((ProfileFields.LAST_NAME));


        if (userData.getCountry().isEmpty())
            validationErrors.add((ProfileFields.COUNTRY));


        if (userData.getBirthDate().isEmpty())
            validationErrors.add((ProfileFields.BIRTHDAY));

        if (userData.getEmail().isEmpty()) {
            validationErrors.add((ProfileFields.EMAIL));
        } else {
            Pattern pattern = Pattern.compile(Globals.EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(userData.getEmail());
            if (!matcher.matches()) validationErrors.add((ProfileFields.EMAIL));
        }

        if (userData.getPassword().length() < 5)
            validationErrors.add((ProfileFields.PASSWORD));


        if (!validationErrors.isEmpty()) {
            eventbus.post(new EventModifyProfileValidationError(validationErrors));
            return false;
        }

        return true;
    }


}
