package hu.zsomii.selfies.interactor.discover;

import android.content.Context;

import java.util.ArrayList;

import javax.inject.Inject;

import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.globals.Globals;
import hu.zsomii.selfies.interactor.base.BaseInteractor;
import hu.zsomii.selfies.interactor.base.event.EventBaseError;
import hu.zsomii.selfies.interactor.session.ISessionInteractor;
import hu.zsomii.selfies.presentation.event.EventDiscoverItemsDownloaded;
import hu.zsomii.selfies.presentation.event.EventLikeSendSucces;
import hu.zsomii.selfies.repository.preferences.ISharedPref;
import hu.zsomii.selfies.stub.Selfie;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class DiscoverInteractor extends BaseInteractor implements IDiscoverInteractor {

    @Inject
    Context context;

    @Inject
    ISharedPref sharedPref;

    @Inject
    ISessionInteractor sessionInteractor;

    public DiscoverInteractor() {
        Selfies.injector().inject(this);
    }


    @Override
    public void getDiscoverItems(int skip, int take) {
        getServices().discoverItems(sessionInteractor.getJWTToken(),skip, take).enqueue(new Callback<ArrayList<Selfie>>() {

            @Override
            public void onResponse(Call<ArrayList<Selfie>> call, Response<ArrayList<Selfie>> response) {
                eventbus.post(new EventDiscoverItemsDownloaded(response.body()));
            }

            @Override
            public void onFailure(Call<ArrayList<Selfie>> call, Throwable t) {
                eventbus.post(new EventBaseError());
            }
        });
    }

    @Override
    public void sendLikeClicked(String imageId, boolean isAlreadyLiked) {

        if (!isAlreadyLiked) {
            getServices().sendLike(sessionInteractor.getJWTToken(),imageId).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    eventbus.post(new EventLikeSendSucces());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    eventbus.post(new EventBaseError());
                }
            });
        } else {
            getServices().deleteLike(sessionInteractor.getJWTToken(),imageId).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    eventbus.post(new EventLikeSendSucces());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                   eventbus.post(new EventBaseError());
                }
            });
        }

    }


}
