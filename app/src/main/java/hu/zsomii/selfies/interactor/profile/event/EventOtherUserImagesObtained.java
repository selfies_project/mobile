package hu.zsomii.selfies.interactor.profile.event;

import java.util.ArrayList;

public class EventOtherUserImagesObtained {

    private ArrayList<String> images;

    public EventOtherUserImagesObtained(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
