package hu.zsomii.selfies.interactor.location.event;

import java.util.ArrayList;

import hu.zsomii.selfies.stub.AvailableLocation;

public class EventGeofenceLocationsDownloaded {

    private ArrayList<AvailableLocation> locations;

    public EventGeofenceLocationsDownloaded(ArrayList<AvailableLocation> locations) {
        this.locations = locations;
    }

    public ArrayList<AvailableLocation> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<AvailableLocation> locations) {
        this.locations = locations;
    }
}
