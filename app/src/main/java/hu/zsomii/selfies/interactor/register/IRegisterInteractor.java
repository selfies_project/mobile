package hu.zsomii.selfies.interactor.register;

import android.graphics.Bitmap;

import hu.zsomii.selfies.interactor.base.IBaseInteractor;
import hu.zsomii.selfies.stub.User;

/**
 * Created by Adam Varga on 3/8/2018.
 */

public interface IRegisterInteractor extends IBaseInteractor {
    void registerUser(User user, Bitmap profilePicture);
    void checkFbToken(String token);
}
