package hu.zsomii.selfies.globals;

import android.annotation.SuppressLint;
import android.support.annotation.VisibleForTesting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Adam Varga on 3/17/2018.
 */

public class Formatter {

    public static SimpleDateFormat dateFormatter;

    @SuppressLint("SimpleDateFormat")
    public Formatter() {
        dateFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
    }

    public static String truncateDateString(String dateString) {
        return dateString.substring(0, 10);
    }

    public static String formatDateString(Date date) {
        SimpleDateFormat dayDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        return dayDateFormatter.format(date);
    }

    public static Date formatDate(String date) {
        SimpleDateFormat dayDateFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy",Locale.ENGLISH);

        try {
            return dayDateFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new Date();
    }

    public static String getDistanceString(float distance) {
        int dist = Math.round(distance);

        if (dist >= 1000)
            return (dist / 1000 + " km");
        else return dist + " m";
    }

    public static SimpleDateFormat getDateFormatter() {
        return dateFormatter;
    }

    public static String formatDayDate(Date date) {
        SimpleDateFormat dayDateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        return dayDateFormatter.format(date);
    }

    public static String formatBirthDayDate(Date date) {
        SimpleDateFormat dayDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        return dayDateFormatter.format(date);
    }

    public static int calculateAge(Date birthdate) {
        Calendar now = Calendar.getInstance();
        Calendar birth = Calendar.getInstance();
        birth.setTimeInMillis(birthdate.getTime());

        if (now.get(Calendar.DAY_OF_YEAR) >= birth.get(Calendar.DAY_OF_YEAR))
            return now.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
        else return now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) - 1;
    }
}
