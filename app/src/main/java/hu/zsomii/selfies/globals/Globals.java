package hu.zsomii.selfies.globals;

/**
 * Created by Adam Varga on 3/7/2018.
 */

public class Globals {
    public static final String EMAIL_PATTERN ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String SERVICE_URL ="http://192.168.0.2:5555/rest/";
    public static final String SERVICE_IMAGE_URL = "http://192.168.0.2:5555/rest/images/getimage?imageId=";

    //Shared pref keys.
    public static final String FIRST_LAUNCH_KEY = "FIRST_LAUNCH";
    public static final String LOGGED_IN_USER = "LOGGED_IN_USER";
    public static final String LAST_LOGGED_IN_USER_EMAIL = "LAST_LOGGED_IN_USER_EMAIL";
    public static final String LAST_LOGGED_IN_USER_PASSWORD = "LAST_LOGGED_IN_USER_PASSWORD";
    public static final String CAMERA_PERMISSION_GRANTED = "CAMERA_PERMISSION_GRANTED";
    public static final String JWT_TOKEN_KEY = "JWT_TOKEN_KEY";

    //Facebook registration keys
    public static final String FB_REG_KEY_EMAIL = "FB_REG_KEY_EMAIL";
    public static final String FB_REG_KEY_FIRST = "FB_REG_KEY_FIRST";
    public static final String FB_REG_KEY_LAST = "FB_REG_KEY_LAST";
    public static final String FB_REG_KEY_TOKEN = "FB_REG_KEY_TOKEN";

    //Geofence keys
    public static final String GEOFENCE_ALREADY_ADDED_IDS = "GEOFENCE_ALREADY_ADDED_IDS";
    public static float GEOFENCE_RADIUS_IN_METERS = 1000f;


}
