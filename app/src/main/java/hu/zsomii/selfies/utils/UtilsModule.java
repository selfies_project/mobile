package hu.zsomii.selfies.utils;

import android.content.Context;

import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

@Module
public class UtilsModule {

    private Context _context;

    public UtilsModule(Context context) {
        _context = context;
    }

    @Provides
    @Singleton
    ModelMapper providesObjectMapper() {
        return new ModelMapper();
    }

    @Provides
    @Singleton
    Context providesContext() {
        return _context;
    }

    @Provides
    @Singleton
    EventBus providesEventBus() {
        return EventBus.getDefault();
    }

}
