package hu.zsomii.selfies.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

import hu.zsomii.selfies.globals.Globals;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by Adam Varga on 2/12/2018.
 */

@GlideModule
public final class GlideImageView extends AppGlideModule {

    public static void loadImage(Context context, String url, ImageView imageView) {
        Log.e("GlideImageView", "loadImage: " + url);
        Glide.with(context).load(url).into(imageView);
    }

    public static void loadImageById(Context context, String id, ImageView imageView) {
        Log.e("GlideImageView", "loadImageById: " + Globals.SERVICE_IMAGE_URL + id);
        String url = Globals.SERVICE_IMAGE_URL + id;
        Glide.with(context).asBitmap().apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).load(url).into(imageView);
    }

    public static void loadBlurImage(Context context, String url, ImageView imageView, int blurRadius) {
        Log.e("GlideImageView", "loadBlurImage: " + url);
        Glide.with(context).load(url).apply(bitmapTransform(new BlurTransformation(blurRadius))).into(imageView);
    }

    public static void loadBlurImageById(Context context, String id, ImageView imageView, int blurRadius) {
        Log.e("GlideImageView", "loadBlurImageById: " + Globals.SERVICE_IMAGE_URL + id);
        String url = Globals.SERVICE_IMAGE_URL + id;
        Glide.with(context).load(url).apply(bitmapTransform(new BlurTransformation(blurRadius))).into(imageView);
    }

    public static void loadLocalImage(Context context, Bitmap bitmap, ImageView imageView) {
        Log.e("GlideImageView", "loadLocalImage: " + bitmap.toString());
        Glide.with(context).load(bitmap).into(imageView);
    }

    public static void loadLocalBlurImage(Context context, Bitmap bitmap, ImageView imageView, int blurRadius) {
        Log.e("GlideImageView", "loadLocalBlurImage: " + bitmap.toString());
        Glide.with(context).load(bitmap).apply(bitmapTransform(new BlurTransformation(blurRadius))).into(imageView);
    }

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}
