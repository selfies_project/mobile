package hu.zsomii.selfies.stub;

import java.util.Date;

public class ToplistUserDto {
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String profilePictureJson;
    private String country;
    private int score;
    private int rank;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfilePictureJson() {
        return profilePictureJson;
    }

    public void setProfilePictureJson(String profilePictureJson) {
        this.profilePictureJson = profilePictureJson;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "ToplistUserDto{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", profilePictureJson='" + profilePictureJson + '\'' +
                ", country='" + country + '\'' +
                ", score=" + score +
                ", rank=" + rank +
                '}';
    }
}
