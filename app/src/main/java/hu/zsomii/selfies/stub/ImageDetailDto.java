package hu.zsomii.selfies.stub;


import java.util.Date;

public class ImageDetailDto
{
    private String imageId;
    private String locationName;
    private Date uploadDate;
    private int likeCount;

    public ImageDetailDto() {
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    @Override
    public String toString() {
        return "ImageDetailDto{" +
                "imageId='" + imageId + '\'' +
                ", locationName='" + locationName + '\'' +
                ", uploadDate=" + uploadDate +
                ", likeCount=" + likeCount +
                '}';
    }
}