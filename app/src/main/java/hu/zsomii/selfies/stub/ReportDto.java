package hu.zsomii.selfies.stub;

import java.util.Date;

public class ReportDto {
    private String relatedPictureId;
    private String reportType;
    private String userFrom;
    private String reason;
    private boolean isResolved;
    private Date reportDate;

    public String getRelatedPictureId() {
        return relatedPictureId;
    }

    public void setRelatedPictureId(String relatedPictureId) {
        this.relatedPictureId = relatedPictureId;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    @Override
    public String toString() {
        return "ReportDto{" +
                "relatedPictureId='" + relatedPictureId + '\'' +
                ", reportType='" + reportType + '\'' +
                ", userFrom='" + userFrom + '\'' +
                ", reason='" + reason + '\'' +
                ", isResolved=" + isResolved +
                ", reportDate=" + reportDate +
                '}';
    }
}
