package hu.zsomii.selfies.stub;

/**
 * Created by Adam Varga on 4/4/2018.
 */

public class ProfileStat {
    private int selfieCount;
    private int score;
    private int likeCount;

    public ProfileStat(int selfieCount, int score, int likeCount) {
        this.selfieCount = selfieCount;
        this.score = score;
        this.likeCount = likeCount;
    }

    public ProfileStat() {
    }

    public int getSelfieCount() {
        return selfieCount;
    }

    public void setSelfieCount(int selfieCount) {
        this.selfieCount = selfieCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }


}
