package hu.zsomii.selfies.stub;

public class LoginUserDto
{
    private String Email;
    private String Password;
    private String PushToken;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPushToken() {
        return PushToken;
    }

    public void setPushToken(String pushToken) {
        PushToken = pushToken;
    }

    @Override
    public String toString() {
        return "LoginUserDto{" +
                "Email='" + Email + '\'' +
                ", Password='" + Password + '\'' +
                ", PushToken='" + PushToken + '\'' +
                '}';
    }
}