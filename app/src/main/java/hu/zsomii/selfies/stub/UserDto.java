package hu.zsomii.selfies.stub;

import java.util.Date;

public class UserDto {
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private Date birthDate;
    private String profilePictureJson;
    private String country;
    private int score;
    private String registerDate;
    private String lastLogin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfilePictureJson() {
        return profilePictureJson;
    }

    public void setProfilePictureJson(String profilePictureJson) {
        this.profilePictureJson = profilePictureJson;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String vountry) {
        this.country = vountry;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", birthDate=" + birthDate +
                ", profilePictureJson='" + profilePictureJson + '\'' +
                ", country='" + country + '\'' +
                ", score=" + score +
                ", registerDate='" + registerDate + '\'' +
                ", lastLogin='" + lastLogin + '\'' +
                '}';
    }
}