package hu.zsomii.selfies.stub;


import java.util.Date;

/**
 * Created by Adam Varga on 3/3/2018.
 */


public class User {

    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String profilePictureJson;
    private String country;
    private int score;
    private String registerDate;
    private String password;
    private String fbAccessToken;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfilePictureJson() {
        return profilePictureJson;
    }

    public void setProfilePictureJson(String profilePictureJson) {
        this.profilePictureJson = profilePictureJson;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", profilePictureJson='" + profilePictureJson + '\'' +
                ", country='" + country + '\'' +
                ", score=" + score +
                ", registerDate='" + registerDate + '\'' +
                ", password='" + password + '\'' +
                ", fbAccessToken='" + fbAccessToken + '\'' +
                '}';
    }
}
