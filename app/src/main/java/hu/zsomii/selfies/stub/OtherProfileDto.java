package hu.zsomii.selfies.stub;

import java.util.Date;

public class OtherProfileDto {
    private String firstName ;
    private String lastName ;
    private Date birthDate ;
    private String profilePictureJson ;
    private String country ;
    private int score ;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfilePictureJson() {
        return profilePictureJson;
    }

    public void setProfilePictureJson(String profilePictureJson) {
        this.profilePictureJson = profilePictureJson;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "OtherProfileDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", profilePictureJson='" + profilePictureJson + '\'' +
                ", country='" + country + '\'' +
                ", score=" + score +
                '}';
    }
}
