package hu.zsomii.selfies.stub;

public class ProfileStatDto {
    private int selfieCount;
    private int score;
    private int likeCount;

    public int getSelfieCount() {
        return selfieCount;
    }

    public void setSelfieCount(int selfieCount) {
        this.selfieCount = selfieCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    @Override
    public String toString() {
        return "ProfileStatDto{" +
                "selfieCount=" + selfieCount +
                ", score=" + score +
                ", likeCount=" + likeCount +
                '}';
    }
}
