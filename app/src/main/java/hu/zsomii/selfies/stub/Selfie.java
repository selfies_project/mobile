package hu.zsomii.selfies.stub;

/**
 * Created by Adam Varga on 4/2/2018.
 */

public class Selfie {
    private String id;
    private String description;
    private String uploadDate;
    private String ownerName;
    private String profilePictureId;
    private String imageId;
    private int likeCount;
    private boolean isAlreadyLiked;

    public Selfie() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getProfilePictureId() {
        return profilePictureId;
    }

    public void setProfilePictureId(String profilePictureId) {
        this.profilePictureId = profilePictureId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public boolean isAlreadyLiked() {
        return isAlreadyLiked;
    }

    public void setAlreadyLiked(boolean alreadyLiked) {
        isAlreadyLiked = alreadyLiked;
    }

    @Override
    public String toString() {
        return "Selfie{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", uploadDate='" + uploadDate + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", profilePictureId='" + profilePictureId + '\'' +
                ", imageId='" + imageId + '\'' +
                ", likeCount=" + likeCount +
                ", isAlreadyLiked=" + isAlreadyLiked +
                '}';
    }
}
