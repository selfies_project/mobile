package hu.zsomii.selfies.push;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import javax.inject.Inject;

import hu.zsomii.selfies.R;
import hu.zsomii.selfies.Selfies;
import hu.zsomii.selfies.presentation.screen.splash.SplashScreenActivity;

public class SelfiesFireBaseMessagingService extends FirebaseMessagingService {

    @Inject
    Context context;

    public SelfiesFireBaseMessagingService() {
        Selfies.injector().inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String message = remoteMessage.getData().get("data");
        if (message != null){
            message = message.substring(message.indexOf(":") + 2, message.length() - 2);

            String CHANNEL_ID = "100234";

            Intent notifyIntent = new Intent(this, SplashScreenActivity.class);
            notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                    this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
            );

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.push_icon)
                    .setContentText(message)
                    .setContentIntent(notifyPendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                long[] vibrationPattern = {100L, 300L, 200L, 1000L};

                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "SELFIES_NOTIFICATION", importance);
                channel.setLightColor(context.getColor(R.color.colorPrimaryDark));
                channel.enableLights(true);
                channel.enableVibration(true);
                channel.setVibrationPattern(vibrationPattern);
                NotificationManager notificationManager = getSystemService(NotificationManager.class);
                assert notificationManager != null;
                notificationManager.createNotificationChannel(channel);

                notificationManager.notify((int) (Math.random() * 100), mBuilder.build());
            }
        }

    }
}
